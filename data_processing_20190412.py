# -*- coding: utf-8 -*-
"""
Created on Mon Mar 25 18:43:44 2019

@author: cxx
"""

import importlib
import sys
import os
import platform

if platform.system() == 'Windows':
    os.chdir("D:/SynologyDrive/Research/STO-Dislocation-Charge")
    newPath = os.getcwd()+'\\code'
elif platform.system() == 'Linux':
    os.chdir('/gpfs/scratch/xuc116/sto-dislocation')
    newPath = os.getcwd()#+'/code'
else:
    os.chdir('/Users/xiaoxingcheng/SynologyDrive/Research/STO-Dislocation-Charge')    
    newPath = os.getcwd()+'/code'

if newPath not in sys.path:
    print("Append new path",newPath)
    sys.path.append(newPath)
    
from STODislocationHeader import *


#%%
def polarStatistics(px,py,pz):
    pLength = np.sqrt(np.square(px)+np.square(py)+np.square(pz))
    abspx = np.absolute(px)
    pxmax = abspx.max()
    pxavg = np.average(abspx)
    abspy = np.absolute(py)
    pymax = abspy.max()
    pyavg = np.average(abspy)
    abspz = np.absolute(pz)
    pzmax = abspz.max()
    pzavg = np.average(abspz)
    
    pmax = pLength.max()
    pavg = np.average(pLength)
    
    return [pmax,pavg,pxmax,pxavg,pymax,pyavg,pzmax,pzavg]
    
def bin2DictList(array):
    return {array[1][i+1]:array[0][i] for i in range(0,len(array[0]))}

def bin2Array(bins):
    return np.array([bins[0][:],bins[1][1:len(bins[0])+1]])
#%%


def processData():
    batchFolders = get_batch_folder_list(params['originalDatDir'])
    for folder in batchFolders:
        var,index=parse_folder_name(folder)
        # flexoCoefficient = var[params['batchKey'][0]]    
        # noiseSeed = var[params['batchKey'][1]]
        root = params['originalDatDir']+'/'+folder
        print(root)
        
        polarName = root + '/' + params['polar']
        polar=muproData.readDat.readDatVector(polarName)[:,0,:,0,:]
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/px',polar[:,:,0])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/py',polar[:,:,1])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/pz',polar[:,:,2])
        
        polarStat = polarStatistics(polar[:,:,0],polar[:,:,1],polar[:,:,2])
        polarStatName = ['pmax','pavg','pxmax','pxavg','pymax','pyavg','pzmax','pzavg']
        for i in range(0,len(polarStat)):
            writeAttr(params['processedDatDir'],params['groupName']+folder,polarStatName[i],polarStat[i])
        
        oxytiltName = root + '/' + params['oxytilt']
        oxytilt=muproData.readDat.readDatVector(oxytiltName)[:,0,:,0,:]
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/qx',oxytilt[:,:,0])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/qy',oxytilt[:,:,1])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/qz',oxytilt[:,:,2])
        
        stressName = root + '/' + params['stress']
        stress=muproData.readDat.readDatScalar(stressName)[:,0,:,:]
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/stress11',stress[:,:,0])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/stress22',stress[:,:,1])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/stress33',stress[:,:,2])    
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/stress23',stress[:,:,3])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/stress13',stress[:,:,4])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/stress12',stress[:,:,5])
        
        stress11GradientY,stress11GradientX = np.gradient(stress[:,:,0],edge_order=1)
        stress33GradientY,stress33GradientX = np.gradient(stress[:,:,2],edge_order=1)
        stress13GradientY,stress13GradientX = np.gradient(stress[:,:,4],edge_order=1)
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/stress11GradientY',stress11GradientY)
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/stress11GradientX',stress11GradientX)
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/stress33GradientY',stress33GradientY)
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/stress33GradientX',stress33GradientX)
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/stress13GradientY',stress13GradientY)
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/stress13GradientX',stress13GradientX)
    
        flexoName = root + '/' + params['flexo']
        flexo=muproData.readDat.readDatScalar(flexoName)[:,0,:,:]
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/flexoField1',flexo[:,:,0])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/flexoField2',flexo[:,:,1])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/flexoField3',flexo[:,:,2])    
    
        phiName = root + '/' + params['phi']
        phi=muproData.readDat.readDatScalar(phiName)[:,0,:,:]
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/electricPotential',phi[:,:,0])
        
        chargeName = root + '/' + params['charge']
        charge=muproData.readDat.readDatScalar(chargeName)[:,0,:,:]
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/boundCharge',charge[:,:,0])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/defectCharge',charge[:,:,1])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/totalCharge',charge[:,:,2])    
        
        displaceName = root + '/' + params['displace']
        displace=muproData.readDat.readDatScalar(displaceName)[:,0,:,:]
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/displacement1',displace[:,:,0])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/displacement2',displace[:,:,1])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/displacement3',displace[:,:,2])    
        
        eigenName = root + '/' + params['eigen']
        eigen=muproData.readDat.readDatScalar(eigenName)[:,0,:,:]
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/eigenStrain11',eigen[:,:,0])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/eigenStrain22',eigen[:,:,1])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/eigenStrain33',eigen[:,:,2])    
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/eigenStrain23',eigen[:,:,3])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/eigenStrain13',eigen[:,:,4])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/eigenStrain12',eigen[:,:,5])
        
        elastFName = root + '/' + params['elasF']
        elastDF=muproData.readDat.readDatScalar(elastFName)[:,0,:,:]
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/elasticDrivingForce1',elastDF[:,:,0])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/elasticDrivingForce2',elastDF[:,:,1])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/elasticDrivingForce3',elastDF[:,:,2])    
        
        elastQFName = root + '/' + params['elasQF']
        elastQDF=muproData.readDat.readDatScalar(elastQFName)[:,0,:,:]
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/elasticQDrivingForce1',elastQDF[:,:,0])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/elasticQDrivingForce2',elastQDF[:,:,1])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/elasticQDrivingForce3',elastQDF[:,:,2])    

        elastEnName = root + '/' + params['elasEn']
        elastEn=muproData.readDat.readDatScalar(elastEnName)[:,0,:,:]
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/elasticEnergy',elastEn[:,:,0])

        elasStrainName = root + '/' + params['elasSt']
        elasStrain=muproData.readDat.readDatScalar(elasStrainName)[:,0,:,:]
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/elasticStrain11',elasStrain[:,:,0])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/elasticStrain22',elasStrain[:,:,1])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/elasticStrain33',elasStrain[:,:,2])    
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/elasticStrain23',elasStrain[:,:,3])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/elasticStrain13',elasStrain[:,:,4])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/elasticStrain12',elasStrain[:,:,5])
        
        strain11GradientY,strain11GradientX = np.gradient(elasStrain[:,:,0],edge_order=1)
        strain33GradientY,strain33GradientX = np.gradient(elasStrain[:,:,2],edge_order=1)
        strain13GradientY,strain13GradientX = np.gradient(elasStrain[:,:,4],edge_order=1)
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/strain11GradientY',strain11GradientY)
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/strain11GradientX',strain11GradientX)
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/strain33GradientY',strain33GradientY)
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/strain33GradientX',strain33GradientX)
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/strain13GradientY',strain13GradientY)
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/strain13GradientX',strain13GradientX)
        
        
        elecEnName = root + '/' + params['elecEn']
        elecEn=muproData.readDat.readDatScalar(elecEnName)[:,0,:,:]
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/electricEnergy',elecEn[:,:,0])
                                                                   
                                                                   

        elecName = root + '/' + params['elecfield']
        elecfield=muproData.readDat.readDatScalar(elecName)[:,0,:,:]
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/electricField1',elecfield[:,:,0])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/electricField2',elecfield[:,:,1])
        backupFile = writeDat2H5(params['processedDatDir'],params['groupName']+folder+'/electricField3',elecfield[:,:,2])
        
        files = get_file_list(params['originalDatDir'])
        for file in files:
            string = loadFile2String(params['originalDatDir']+'/'+file)
            writeAttr(params['processedDatDir'],params['groupName'],file,string)

            
            
#%%
params={}

params['imgDir']='./img'
params['originalDatDir']='./screw100'
params['processedDatDir']='./20190812_STO_Dislocation_Charge_Screw.h5'
# params['originalDatDir']='./raw_data/burg100'
# params['processedDatDir']='./20190507_STO_Dislocation_Charge.h5'
# params['groupName']='/burg100_no_charge/'
# params['curDir']="D:/SynologyDrive/Research/STO-Dislocation-Flexo/paper"
params['start'] = 2000
params['polar'] = 'Polar.00002000.dat'
params['oxytilt'] = 'OctaTilt.00002000.dat'
params['stress'] = 'Stress.00002001.dat'
params['flexo'] = 'Eflexo.00002001.dat'
params['phi'] = 'Elec_Phi.00002001.dat'
params['charge'] = 'Charges.00002001.dat'
params['displace'] = 'Displace.00002001.dat'
params['eigen'] = 'Eigen_St.00002001.dat'
params['elasF'] = 'Elas_For.00002001.dat'
params['elasQF'] = 'ElasQFor.00002001.dat'
params['elasEn'] = 'Elast_En.00002001.dat'
params['elasSt'] = 'Elast_St.00002001.dat'
params['elecEn'] = 'Elect_En.00002001.dat'
params['elecfield'] = 'Elefield.00002001.dat'

params['nx']=512
params['ny']=1
params['nz']=512
#params['bins']=list(np.linspace(0,0.3,101))
params['bins']=list(np.linspace(0,0.5,101))
#params['bins']=[0,0.01,0.04,0.08,0.12,0.16,0.20,0.24,0.28]
params['batchKey']=['&MATERIAL.FLEXOCON','&DISLOCATION1.DEFECTCHARGE','pqrseed']

#%% This will collect the data into the h5 file, should only be run once

#folderList = get_folder_list('./raw_data')
#folderList = ['burg_100','burg_110']
#for folder in folderList:
#    params['originalDatDir']='./raw_data/'+folder
#    params['groupName']=folder+'/'
#    processData()

#%%
params['originalDatDir']='./screw100'
params['groupName']='/screw100_with_charge/'
params['batchKey']=['&MATERIAL.FLEXOCON','&PNOISEED','&QNOISEED']
processData()


#%% fix the statistics
#dataAll= h5py.File('20190325_STO_dislocation_flexo.h5','r')
group_list = getH5List(params['processedDatDir'])

#dataAll.close()
for group in group_list:
    print(group)
    oneGroupList = getH5GroupList(params['processedDatDir'],'/'+group)
    for dataName in oneGroupList:
        print(dataName)
#        data= oneGroup[dataName]
        px = readDatFromH5(params['processedDatDir'],'/'+group+'/'+dataName+'/px')
        py = readDatFromH5(params['processedDatDir'],'/'+group+'/'+dataName+'/py')
        pz = readDatFromH5(params['processedDatDir'],'/'+group+'/'+dataName+'/pz')
        polarStat = polarStatistics(px,py,pz)
        polarStatName = ['pmax','pavg','pxmax','pxavg','pymax','pyavg','pzmax','pzavg']
        for i in range(0,len(polarStat)):
            writeAttr(params['processedDatDir'],'/'+group+'/'+dataName,polarStatName[i],polarStat[i])
#        print(polarStat)
        pxBins = np.histogram(np.absolute(px),bins=params['bins'])
        pyBins = np.histogram(np.absolute(py),bins=params['bins'])
        pzBins = np.histogram(np.absolute(pz),bins=params['bins'])
        pBins = np.histogram(np.sqrt(np.square(px))+np.square(py)+np.square(pz),bins=params['bins'])

#        dic = {'px':bin2DictList(pxBins),'py':bin2DictList(pyBins),'pz':bin2DictList(pzBins)}
        pxArray = bin2Array(pxBins)
        pyArray = bin2Array(pyBins)
        pzArray = bin2Array(pzBins)
        pArray = bin2Array(pBins)
        writeDat2H5(params['processedDatDir'],'/'+group+'/'+dataName+'/pxBins',pxArray)
        writeDat2H5(params['processedDatDir'],'/'+group+'/'+dataName+'/pyBins',pyArray)
        writeDat2H5(params['processedDatDir'],'/'+group+'/'+dataName+'/pzBins',pzArray)
        writeDat2H5(params['processedDatDir'],'/'+group+'/'+dataName+'/pBins',pArray)
#        df = pd.DataFrame.from_dict(di)

#%% collect the max and average statistics from each simulation into the parent folder
group_list = getH5GroupList(params['processedDatDir'])
for group in group_list:
    print(group,'after')
    oneGroupList = getH5GroupList(params['processedDatDir'],'/'+group)
    polarStatName = ['pmax','pavg','pxmax','pxavg','pymax','pyavg','pzmax','pzavg']
    polarStat = {}
    for dataName in oneGroupList:
        var,index=parse_folder_name(dataName)
        firstName = '_'.join(var['&MATERIAL.FLEXOCON']+[var['&DISLOCATION1.DEFECTCHARGE']])
        if firstName not in polarStat:
            polarStat[firstName]={}
            for name in polarStatName:
                polarStat[firstName][name] = []
        for name in polarStatName:
            if len(polarStat[firstName][name]) >= 5:
                polatStat[firstName][name] = []
            newAttr = readAttr(params['processedDatDir'],group+'/'+dataName,name)
            polarStat[firstName][name].append(newAttr)
    
    for firstKey in polarStat.keys():
        for typeKey in polarStat[firstKey]:
            writeAttr(params['processedDatDir'],group,typeKey+"_"+firstKey,polarStat[firstKey][typeKey])

#%% calculate the bins for histogram
group_list = getH5GroupList(params['processedDatDir'])
for group in group_list:
    oneGroupList = getH5GroupList(params['processedDatDir'],'/'+group)
    polarBinName = ['pxBins','pyBins','pzBins','pBins']
    polarBin = {}
    for dataName in oneGroupList:
        var,index=parse_folder_name(dataName)
        firstName = '_'.join(var['&MATERIAL.FLEXOCON']+[var['&DISLOCATION1.DEFECTCHARGE']])
        if firstName not in polarBin:
            polarBin[firstName] = {}
            for name in polarBinName:
                polarBin[firstName][name]=np.array(params['bins'][1:])
                
        for name in polarBinName:
            if polarBin[firstName][name].shape[0] >= 6:
                polarBin[firstName][name]=np.array(params['bins'][1:])
            newDat = readDatFromH5(params['processedDatDir'],group+'/'+dataName+'/'+name)
            polarBin[firstName][name] = np.vstack((polarBin[firstName][name],newDat[0]))
    
    for firstKey in polarBin.keys():
        for typeKey in polarBin[firstKey]:
            writeDat2H5(params['processedDatDir'],group+'/'+typeKey+"_"+firstKey,polarBin[firstKey][typeKey])
#%%
#params['originalDatDir']='./raw_data/burg100'
#params['groupName']='/burg100_no_charge/'
#params['batchKey']=['&MATERIAL.FLEXOCON','&PNOISEED','&QNOISEED']
#processData()
##%%
#params['originalDatDir']='./raw_data/burg110'
#params['groupName']='/burg110_no_charge/'
#params['batchKey']=['&MATERIAL.FLEXOCON','&PNOISEED','&QNOISEED']
#processData()
#
##%%
#params['originalDatDir']='./raw_data/burg100_with_charge'
#params['groupName']='/burg100_with_charge/'
#params['batchKey']=['&DISLOCATION1.DEFECTCHARGE','&PNOISEED','QNOISEED']
#processData()
#
##%%
#
#params['originalDatDir']='./raw_data/burg110_with_charge'
#params['groupName']='/burg110_with_charge/'
#params['batchKey']=['&DISLOCATION1.DEFECTCHARGE','&PNOISEED','QNOISEED']
#processData()
#
##%%
#params['originalDatDir']='./raw_data/burg100_with_charge_no_flexo'
#params['groupName']='/burg100_with_charge_no_flexo/'
#params['batchKey']=['&DISLOCATION1.DEFECTCHARGE','&PNOISEED','QNOISEED']
#processData()
#
##%%
#params['originalDatDir']='./raw_data/burg110_with_charge_no_flexo'
#params['groupName']='/burg110_with_charge_no_flexo/'
#params['batchKey']=['&DISLOCATION1.DEFECTCHARGE','&PNOISEED','QNOISEED']
#processData()

#%%
 #%%
  
#%%
