# -*- coding: utf-8 -*-
"""
Created on Wed Mar 27 19:47:09 2019

@author: cxx
"""

import importlib
import sys
import os
import platform

if platform.system() == 'Windows':
    os.chdir("D:/SynologyDrive/Research/STO-Dislocation-Flexo/paper")
    newPath = os.getcwd()+'\\code'
elif platform.system() == 'Linux':
    os.chdir('/gpfs/scratch/xuc116/sto-dislocation')
    newPath = os.getcwd()+'/code'
else:
    os.chdir('/Users/xiaoxingcheng/SynologyDrive/Research/STO-Dislocation-Flexo/paper')    
    newPath = os.getcwd()+'/code'

if newPath not in sys.path:
    print("Append new path")
    sys.path.append(newPath)
    
from STODislocationHeader import *

#%%
def ladder(ax,texts,centerX,centerY,width,height,fs=15):

#    ax.add_patch(pathpatch)
    
    rect = Rectangle([centerX-width/2.0,centerY-height/2.0],width,height,facecolor='None',edgecolor='green')
    ax.add_patch(rect)
    
    dy = height / len(texts)
    refy = centerY + height/2.0
    for i in range(0,len(texts)-1):
        refy = refy - dy
        line = Line2D([centerX-width/2.0,centerX+width/2.0],[refy,refy])
        ax.add_line(line)    
        ax.text(centerX,refy + dy/2.0,texts[i],horizontalalignment='center',verticalalignment='center',fontsize=fs)
        
    ax.text(centerX,refy - dy/2.0,texts[-1],horizontalalignment='center',verticalalignment='center',fontsize=fs)

#%%
params={}
params['imgDir']='./img-new/sup1'
params['imgFile']='./img-new/sup1.svg'
params['ImgDir']='\\img-new\\sup1\\'
params['datDir110']='/burg_110/01+&MATERIAL.FLEXOCON_0_0_0+&PNOISEED_1+QNOISEED_1/'
params['datDir100']='/burg_100/01+&MATERIAL.FLEXOCON_0_0_0+&PNOISEED_1+QNOISEED_1/'

touch(params['imgDir'])
params['captionFile'] = './img-new/sup1.md'
params['caption'] = """

## Supplementary 1
![Supplementary 1](sup1.svg)

The simulation setup.
(a) a total of 80 jobs, with 8 sets of flexoelectric coefficients, 2 sets of burgers vector, and 5 different random seeds were calculated.
(b) (100) dislocation setup
(c) (110) dislocation setup
"""
#%%

dataAll= h5py.File('20190422_STO_dislocation_flexo_mech.h5','r')

# a s11
s11_100 = np.array(dataAll[params['datDir100']+'stress11'])
# b s33
s33_100 = np.array(dataAll[params['datDir100']+'stress33'])
# c s13
s13_100 = np.array(dataAll[params['datDir100']+'stress13'])

# a s11
s11_110 = np.array(dataAll[params['datDir110']+'stress11'])
# b s33
s33_110 = np.array(dataAll[params['datDir110']+'stress33'])
# c s13
s13_110 = np.array(dataAll[params['datDir110']+'stress13'])

stressRange=[min(s11_100.min(),s33_100.min(),s13_100.min()),max(s11_100.max(),s33_100.max(),s13_100.max())]

dataAll.close()

#%% supplementary simulation setup illustration
fig = plt.figure(figsize=(10,5))
ax = fig.add_axes([0,0,1,1])
ax.axis('off')

firstRow=0.41
firstCol=0.18
secondRow=0.2
secondCol=0.48
thirdCol=0.73
offset=0.49
titleSize = 18

ax.text(0.05,firstRow,'',horizontalalignment='center',verticalalignment='center',fontsize = titleSize)
ax.text(firstCol,firstRow+offset,'Flexoelectric \n coefficient \n $V_{1111},V_{1122},V_{1212}$ (V)',horizontalalignment='center',verticalalignment='center',fontsize = titleSize)
ladder(ax,['Case 1: 0.0, 0.0, 0.0','Case 2: 0.8, 2.6, 2.2','Case 3: 1.0, 0.0, 0.0','Case 4: 0.0, 1.0, 0.0','Case 5: 0.0, 0.0, 1.0','Case 6: 2.0, 0.0, 0.0','Case 7: 0.0, 2.0, 0.0','Case 8: 0.0, 0.0, 2.0'], firstCol,firstRow,0.3,0.8)

ax.text((firstCol+secondCol)/2.0+(0.3-0.2)/4.0,firstRow,r'$\times$',ha='center',va='center',fontsize = titleSize)
ax.text(secondCol,firstRow+offset,'Burgers \n vector (nm)',horizontalalignment='center',verticalalignment='center',fontsize = titleSize)
ladder(ax,['(0.4, 0.0, 0.0)','(0.2, 0.0, 0.2)'], secondCol,firstRow,0.2,0.3)

ax.text((thirdCol+secondCol)/2.0,firstRow,r'$\times$',ha='center',va='center',fontsize = titleSize)
ax.text(thirdCol,firstRow+offset,'Random \n seed',horizontalalignment='center',verticalalignment='center',fontsize = titleSize)
ladder(ax,['1','2','3','4','5'], thirdCol,firstRow,0.15,0.75)

ax.text(0.88,firstRow,r'$=\;80\;\mathrm{jobs}$',ha='center',va='center',fontsize = titleSize)




#ax.text(0.05,secondRow,'Group 2',horizontalalignment='center',verticalalignment='center',fontsize = titleSize)
#ax.text(firstCol,secondRow+offset,'Defect\n  charges ' r'$(\frac{C}{m^3})$',horizontalalignment='center',verticalalignment='center',fontsize = titleSize)
#ladder(ax,['0.2','0.4','0.6','0.8','1.0'], firstCol,secondRow,0.2,0.35)
#
#ax.text((firstCol+secondCol)/2.0,secondRow,r'$\times$',ha='center',va='center',fontsize = titleSize)
#ax.text(secondCol,secondRow+offset,'Burgers \n vector (nm)',horizontalalignment='center',verticalalignment='center',fontsize = titleSize)
#ladder(ax,['(0.4, 0.0, 0.0)','(0.2, 0.0, 0.2)'], secondCol,secondRow,0.2,0.2)
#
#ax.text((thirdCol+secondCol)/2.0,secondRow,r'$\times$',ha='center',va='center',fontsize = titleSize)
#ax.text(thirdCol,secondRow+offset,'Random \n seed',horizontalalignment='center',verticalalignment='center',fontsize = titleSize)
#ladder(ax,['1','2','3','4','5'], thirdCol,secondRow,0.2,0.35)
#ax.text(0.95,secondRow,r'$=\;50\;\mathrm{jobs}$',ha='center',va='center',fontsize = titleSize)

plt.savefig(params['imgDir']+'/sup1_1.svg')

#%%
cmapGlob = 'inferno'

patch = getScaleBar(128,40,150,20,'60nm',color='white',lw=5,fs=30,padding=[20,20],bg = 'None')
heatPlot_noEdge(s11_100,stressRange,5,params['imgDir']+'/figure_sup1a_stress11.svg',cmapGlob=cmapGlob,disAngle=-1,artists = patch)

patch = []
heatPlot_noEdge(s11_110,stressRange,5,params['imgDir']+'/figure_sup1b_stress11.svg',cmapGlob=cmapGlob,disAngle=-1,artists = patch)

#%%

#copyfile(params['imgDir']+'/sup1.svg',params['imgFile'])
#texEquation(r'$\sigma_{11}$',0.8,0.4,params['imgDir']+'/sigma11.svg',20)
#texEquation(r'$\sigma_{33}$',0.8,0.4,params['imgDir']+'/sigma33.svg',20)
#%%
figureName=[['sup1_1.svg',''],
            ['figure_sup1a_stress11.svg','figure_sup1b_stress11.svg']]

label = [os.getcwd()+'\\img-new\\'+ name+'.svg' for name in ['a','b','c','d','e','f']]
labelTransparent = [os.getcwd()+'\\img-new\\'+ name+'.svg' for name in ['aa','bb','cc','dd','ee','ff']]


figureWidth = 364*0.75+10
figureHeight = 205
textSize = "20"
textBase = 18
textLeft = 5
boxSize="25"
offsetY = 364*0.75
offsetX = 5
labelFont = 'DejaVu Sans'
labelWeight = 'normal'
figures = [[os.getcwd()+params['ImgDir']+figureName[i][j] for j in range(0,2)] for i in range(0,2)]
sc.Figure('15cm','15cm',
          sc.Panel(
                  sc.SVG(figures[0][0]).scale(0.75),
                  sc.SVG(label[0]).move(-1,-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[1][0]).scale(0.75).move(figureWidth*0+offsetX,offsetY),
                  sc.SVG(label[1]).move(-1,offsetY-1)
                  ),          
          sc.Panel(
                  sc.SVG(figures[1][1]).scale(0.75).move(figureWidth*1+offsetX,offsetY),
                  sc.SVG(label[2]).move(figureWidth-1,offsetY-1)
                  )
          ).save(params['imgFile'])
#          sc.Grid(20,20)
          

#%%
fixSVGASCII(params['imgFile'])

with open(params['captionFile'],'w') as file:
    file.write(params['caption'])






























#%%
#    vertices = []
#    codes = []
#    codes = [Path.MOVETO] + [Path.LINETO]*3 + [Path.CLOSEPOLY]
#    x1 = centerX-width/2.0
#    y1 = centerY-height/2.0
#    x2 = centerX-width/2.0
#    y2 = centerY+height/2.0
#    x3 = centerX+width/2.0
#    y3 = centerY+height/2.0
#    x4 = centerX+width/2.0
#    y4 = centerY-height/2.0
#    vertices = [(x1, y1), (x2, y2), (x3, y3), (x4, y4), (0, 0)]
#    path = Path(vertices, codes)
#    pathpatch = PathPatch(path, facecolor='None', edgecolor='green')
#%%
#def ladder(svg,label,texts,centerX,centerY,nx,ny):
#    x = centerX - nx/2.0
#    y = centerY - ny/2.0
#    layer = svg.layer(label=label)
#    layer["sodipodi:insensitive"] = "true"
#    svg.add(layer)
#    nn = len(texts)
#    dy = ny/float(nn)
#
#    rect = svg.rect((x,y),(nx,ny),
#                    fill='#ffffff',
#                     fill_opacity='0',
#                     stroke=svgwrite.rgb(10, 10, 16, '%'),
#                     stroke_width='2')
#    layer.add(rect)
#    refY = y
#
#    for i in range(0,nn-1):
#        text = texts[i]
#        refY = refY + dy
#        line = svg.line((x,refY),(x+nx,refY),stroke='black',stroke_width='2')
#        txt = svg.text(text,insert=(x+nx/2.0,refY-dy/2.0),font_size='15',fill='red',alignment_baseline="middle", text_anchor="middle")
#        layer.add(line)
#        layer.add(txt)
#        
#    txt = svg.text(text,insert=(x+nx/2.0,refY+dy/2.0),font_size='15',fill='red',alignment_baseline="middle", text_anchor="middle")
#
#    layer.add(txt)
#    return svg
#
#
##,textLength=nx*0.8,
#
##%%
#
#svg = InkscapeDrawing('inkscape-test.svg', profile='full', size=(840, 480))
#
#layer1 = svg.layer(label="group1")
#layer1["sodipodi:insensitive"] = "true"
#svg.add(layer1)
#
#txt = svg.text('Flexoelectric coefficient',insert=(200,10),font_size='20',font_weight='bold',fill='red',alignment_baseline="middle", text_anchor="middle")
#layer1.add(txt)
#svg = ladder(svg,'Flexoelectric coefficient',['0.0, 0.0, 0.0','0.8, 2.6, 2.2','1.0, 0.0, 0.0','0.0, 1.0, 0.0','0.0, 0.0, 1.0'],200,100,100,150)
#txt = svg.text('x',insert=(300,100),font_size='20',font_weight='bold',fill='red',alignment_baseline="middle", text_anchor="middle")
#layer1.add(txt)
#
#txt = svg.text('Burgers vector',insert=(400,10),font_size='20',font_weight='bold',fill='red',alignment_baseline="middle", text_anchor="middle")
#layer1.add(txt)
#svg = ladder(svg,'Burgers vector',['(0.4, 0.0, 0.0)','(0.2, 0.0, 0.2)'],400,100,100,60)
#
#txt = svg.text('Initial seed',insert=(600,10),font_size='20',font_weight='bold',fill='red',alignment_baseline="middle", text_anchor="middle")
#layer1.add(txt)
#svg = ladder(svg,'Initial seed',['1','2','3','4','5'],600,100,50,150)
#  
#group1 = svg.text('Group 1', insert=(100, 100), font_size='15', fill='red', alignment_baseline="middle", text_anchor="middle")
#layer1.add(group1)
#
#
#
#layer2 = svg.layer(label="group2")
#layer2["sodipodi:insensitive"] = "true"
#svg.add(layer2)
#svg = ladder(svg,'layer2_ladder',['a','b','c'],200,300,50,100)
#
#
#group2 = svg.text('Group 2', insert=(100, 300), font_size='15', fill='red',alignment_baseline="middle", text_anchor="middle")
#layer2.add(group2)
#
#
#svg.save()