# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 14:17:47 2019

@author: cxx
"""
import importlib
import sys
import os
import platform

if platform.system() == 'Windows':
    os.chdir("D:/SynologyDrive/Research/STO-Dislocation-Flexo/paper")
    newPath = os.getcwd()+'\\code'
elif platform.system() == 'Linux':
    os.chdir('/gpfs/scratch/xuc116/sto-dislocation')
    newPath = os.getcwd()+'/code'
else:
    os.chdir('/Users/xiaoxingcheng/SynologyDrive/Research/STO-Dislocation-Flexo/paper')    
    newPath = os.getcwd()+'/code'

if newPath not in sys.path:
    print("Append new path")
    sys.path.append(newPath)
    
from STODislocationHeader import *



#%%
def splitData(array,mi,ma):
    index = array[0]
    avg = np.average(array[1:],axis=0)
    std = np.std(array[1:],axis=0)
    return index[mi:ma],avg[mi:ma],std[mi:ma]

def addOneLine(ax,data,mi,ma):
    index,avg,std = splitData(data,mi,ma)
    ax.plot(index,avg)
    ax.fill_between(index,avg-std,avg+std)
    return ax

def binerize(pAngle,pLength):
    df = pd.DataFrame.from_dict({'angle':pAngle.flatten(),'length':pLength.flatten()})
    df.angle = df.angle*180/np.pi
    dfgroup = df.groupby(pd.cut(df.angle,np.arange(-180,181,bin_width))).length.sum()
    return dfgroup.values
#%%
params={}

params['imgDir']='./img-new/figure4'
params['imgFile']='./img-new/figure4.svg'
params['ImgDir']='\\img-new\\figure4\\'
params['datDir']='/burg_100_with_charge_no_flexo/'
params['start'] = 2000
#params['datFold'] = '21+&DISLOCATION1.DEFECTCHARGE_1.0+&PNOISEED_1+QNOISEED_1'
params['datFold'] = '01+&DISLOCATION1.DEFECTCHARGE_0.2+&PNOISEED_1+QNOISEED_1'
params['nx']=512
params['ny']=1
params['nz']=512

params['xrange']=[244,268]
params['yrange']=[114,138]

params['disLocX']=0.49
params['disLocY']=0.49

params['Xrange']=[115,139]
params['Yrange']=[115,139]

params['batchKey']=['&MATERIAL.FLEXOCON','&PNOISEED','&QNOISEED']

touch(params['imgDir'])

x1=params['xrange'][0]
x2=params['xrange'][1]
y1=params['yrange'][0]
y2=params['yrange'][1]

x3=params['Xrange'][0]
x4=params['Xrange'][1]
y3=params['Yrange'][0]
y4=params['Yrange'][1]




params['captionFile'] = './img-new/figure4.md'
params['caption'] = """

## Figure 4
![Figure 4](figure4.svg)

The polarization distribution with different flexoelectric coefficients (case 1, 2, 3, 4, 5) for (110) dislocation.
(a), (b), (d), (e), and (f) show the polarization vector (quiver) and magnitude (heat map) for case 1, 2, 3, 4 and 5 respectively.
(c) Angular histogram of polarization magnitude. Polarization is divided into 36 bins based on their orientaton. x value is the orientation angle of each bin with respect to (100) direction, and y value is the summation of polarization magnitude within each bin.
A peak in (c) means it is a preferred orientation of polarization distribution. The vectors in (a), (d), and (e) are enlarged multiple times, indicated at the top right corner.

"""



bin_width = 10

patch = getScaleBar(4,1,5,1.5,'2nm',color='white',lw=5,fs=30,padding=[1,1],bg = 'None')

#%% open the file, important always remember to close it
#dataAll= h5py.File('20190325_STO_dislocation_flexo.h5','r')
dataAll= h5py.File('20190422_STO_dislocation_flexo_mech.h5','r')


#100 no charge no flexo
px_100_no = np.array(dataAll['/burg_100/01+&MATERIAL.FLEXOCON_0_0_0+&PNOISEED_1+QNOISEED_1/px'][x1:x2,y1:y2])
pz_100_no = np.array(dataAll['/burg_100/01+&MATERIAL.FLEXOCON_0_0_0+&PNOISEED_1+QNOISEED_1/pz'][x1:x2,y1:y2])
pAngle_100_no = np.arctan2(pz_100_no,px_100_no)
pLength_100_no = np.sqrt(np.square(np.array(px_100_no))+np.square(np.array(pz_100_no)))
p_100_no_bins = binerize(pAngle_100_no,pLength_100_no)


#100 1 0 0 no flexo
px_100_1_0_0 = np.array(dataAll['/burg_100/06+&MATERIAL.FLEXOCON_1_0_0+&PNOISEED_1+QNOISEED_1/px'][x1:x2,y1:y2])
pz_100_1_0_0 = np.array(dataAll['/burg_100/06+&MATERIAL.FLEXOCON_1_0_0+&PNOISEED_1+QNOISEED_1/pz'][x1:x2,y1:y2])
pAngle_100_1_0_0 = np.arctan2(pz_100_1_0_0,px_100_1_0_0)
pLength_100_1_0_0 = np.sqrt(np.square(np.array(px_100_1_0_0))+np.square(np.array(pz_100_1_0_0)))
p_100_1_0_0_bins = binerize(pAngle_100_1_0_0,pLength_100_1_0_0)



#110 f12=1 flexo
px_100_0_1_0 = np.array(dataAll['/burg_100/16+&MATERIAL.FLEXOCON_0_1_0+&PNOISEED_1+QNOISEED_1/px'][x1:x2,y1:y2])
pz_100_0_1_0 = np.array(dataAll['/burg_100/16+&MATERIAL.FLEXOCON_0_1_0+&PNOISEED_1+QNOISEED_1/pz'][x1:x2,y1:y2])
pAngle_100_0_1_0 = np.arctan2(pz_100_0_1_0,px_100_0_1_0)
pLength_100_0_1_0 = np.sqrt(np.square(np.array(px_100_0_1_0))+np.square(np.array(pz_100_0_1_0)))
p_100_0_1_0_bins = binerize(pAngle_100_0_1_0,pLength_100_0_1_0)


#110 f44=1 flexo
px_100_0_0_1 = np.array(dataAll['/burg_100/26+&MATERIAL.FLEXOCON_0_0_1+&PNOISEED_1+QNOISEED_1/px'][x1:x2,y1:y2])
pz_100_0_0_1 = np.array(dataAll['/burg_100/26+&MATERIAL.FLEXOCON_0_0_1+&PNOISEED_1+QNOISEED_1/pz'][x1:x2,y1:y2])
pAngle_100_0_0_1 = np.arctan2(pz_100_0_0_1,px_100_0_0_1)
pLength_100_0_0_1 = np.sqrt(np.square(np.array(px_100_0_0_1))+np.square(np.array(pz_100_0_0_1)))
p_100_0_0_1_bins = binerize(pAngle_100_0_0_1,pLength_100_0_0_1)

#110 f44=1 flexo
px_100_008_26_22 = np.array(dataAll['/burg_100/36+&MATERIAL.FLEXOCON_0.08_2.6_2.2+&PNOISEED_1+QNOISEED_1/px'][x1:x2,y1:y2])
pz_100_008_26_22 = np.array(dataAll['/burg_100/36+&MATERIAL.FLEXOCON_0.08_2.6_2.2+&PNOISEED_1+QNOISEED_1/pz'][x1:x2,y1:y2])
pAngle_100_008_26_22 = np.arctan2(pz_100_008_26_22,px_100_008_26_22)
pLength_100_008_26_22 = np.sqrt(np.square(np.array(px_100_008_26_22))+np.square(np.array(pz_100_008_26_22)))
p_100_008_26_22_bins = binerize(pAngle_100_008_26_22,pLength_100_008_26_22)





#110 no charge no flexo
px_110_no = np.array(dataAll['/burg_110/01+&MATERIAL.FLEXOCON_0_0_0+&PNOISEED_1+QNOISEED_1/px'][x3:x4,y3:y4])
pz_110_no = np.array(dataAll['/burg_110/01+&MATERIAL.FLEXOCON_0_0_0+&PNOISEED_1+QNOISEED_1/pz'][x3:x4,y3:y4])
pAngle_110_no = np.arctan2(px_110_no,pz_110_no)
pLength_110_no = np.sqrt(np.square(np.array(px_110_no))+np.square(np.array(pz_110_no)))
p_110_no_bins = binerize(pAngle_110_no,pLength_110_no)

#110 f11=1 flexo
px_110_1_0_0 = np.array(dataAll['/burg_110/06+&MATERIAL.FLEXOCON_1_0_0+&PNOISEED_1+QNOISEED_1/px'][x3:x4,y3:y4])
pz_110_1_0_0 = np.array(dataAll['/burg_110/06+&MATERIAL.FLEXOCON_1_0_0+&PNOISEED_1+QNOISEED_1/pz'][x3:x4,y3:y4])
pAngle_110_1_0_0 = np.arctan2(px_110_1_0_0,pz_110_1_0_0)
pLength_110_1_0_0 = np.sqrt(np.square(np.array(px_110_1_0_0))+np.square(np.array(pz_110_1_0_0)))
p_110_1_0_0_bins = binerize(pAngle_110_1_0_0,pLength_110_1_0_0)

#110 f12=1 flexo
px_110_0_1_0 = np.array(dataAll['/burg_110/16+&MATERIAL.FLEXOCON_0_1_0+&PNOISEED_1+QNOISEED_1/px'][x3:x4,y3:y4])
pz_110_0_1_0 = np.array(dataAll['/burg_110/16+&MATERIAL.FLEXOCON_0_1_0+&PNOISEED_1+QNOISEED_1/pz'][x3:x4,y3:y4])
pAngle_110_0_1_0 = np.arctan2(px_110_0_1_0,pz_110_0_1_0)
pLength_110_0_1_0 = np.sqrt(np.square(np.array(px_110_0_1_0))+np.square(np.array(pz_110_0_1_0)))
p_110_0_1_0_bins = binerize(pAngle_110_0_1_0,pLength_110_0_1_0)

#110 f44=1 flexo
px_110_0_0_1 = np.array(dataAll['/burg_110/26+&MATERIAL.FLEXOCON_0_0_1+&PNOISEED_1+QNOISEED_1/px'][x3:x4,y3:y4])
pz_110_0_0_1 = np.array(dataAll['/burg_110/26+&MATERIAL.FLEXOCON_0_0_1+&PNOISEED_1+QNOISEED_1/pz'][x3:x4,y3:y4])
pAngle_110_0_0_1 = np.arctan2(px_110_0_0_1,pz_110_0_0_1)
pLength_110_0_0_1 = np.sqrt(np.square(np.array(px_110_0_0_1))+np.square(np.array(pz_110_0_0_1)))
p_110_0_0_1_bins = binerize(pAngle_110_0_0_1,pLength_110_0_0_1)

#110 f44=1 flexo
px_110_008_26_22 = np.array(dataAll['/burg_110/36+&MATERIAL.FLEXOCON_0.08_2.6_2.2+&PNOISEED_1+QNOISEED_1/px'][x3:x4,y3:y4])
pz_110_008_26_22 = np.array(dataAll['/burg_110/36+&MATERIAL.FLEXOCON_0.08_2.6_2.2+&PNOISEED_1+QNOISEED_1/pz'][x3:x4,y3:y4])
pAngle_110_008_26_22 = np.arctan2(px_110_008_26_22,pz_110_008_26_22)
pLength_110_008_26_22 = np.sqrt(np.square(np.array(px_110_008_26_22))+np.square(np.array(pz_110_008_26_22)))
p_110_008_26_22_bins = binerize(pAngle_110_008_26_22,pLength_110_008_26_22)


#pxBins_100_no = np.array(dataAll['/burg_100/pxBins_0.08_2.6_2.2'])
#pzBins_100_no = np.array(dataAll['/burg_100/pzBins_0.08_2.6_2.2'])
#pBins_100_no = np.array(dataAll['/burg_100/pBins_0.08_2.6_2.2'])
#
#pxBins_110_no = np.array(dataAll['/burg_110/pxBins_0.08_2.6_2.2'])
#pzBins_110_no = np.array(dataAll['/burg_110/pzBins_0.08_2.6_2.2'])
#pBins_110_no = np.array(dataAll['/burg_110/pBins_0.08_2.6_2.2'])
#
#pxBins_100_1_0_0 = np.array(dataAll['/burg_100/pxBins_1_0_0'])
#pzBins_100_1_0_0 = np.array(dataAll['/burg_100/pzBins_1_0_0'])
#pBins_100_1_0_0 = np.array(dataAll['/burg_100/pBins_1_0_0'])
#
#pxBins_110_1_0_0 = np.array(dataAll['/burg_110/pxBins_1_0_0'])
#pzBins_110_1_0_0 = np.array(dataAll['/burg_110/pzBins_1_0_0'])
#pBins_110_1_0_0 = np.array(dataAll['/burg_110/pBins_1_0_0'])
#
#pxBins_100_0_1_0 = np.array(dataAll['/burg_100/pxBins_0_1_0'])
#pzBins_100_0_1_0 = np.array(dataAll['/burg_100/pzBins_0_1_0'])
#pBins_100_0_1_0 = np.array(dataAll['/burg_100/pBins_0_1_0'])
#
#pxBins_110_0_1_0 = np.array(dataAll['/burg_110/pxBins_0_1_0'])
#pzBins_110_0_1_0 = np.array(dataAll['/burg_110/pzBins_0_1_0'])
#pBins_110_0_1_0 = np.array(dataAll['/burg_110/pBins_0_1_0'])
#
#pxBins_100_0_0_1 = np.array(dataAll['/burg_100/pxBins_0_1_0'])
#pzBins_100_0_0_1 = np.array(dataAll['/burg_100/pzBins_0_1_0'])
#pBins_100_0_0_1 = np.array(dataAll['/burg_100/pBins_0_1_0'])
#
#pxBins_110_0_0_1 = np.array(dataAll['/burg_110/pxBins_0_0_1'])
#pzBins_110_0_0_1 = np.array(dataAll['/burg_110/pzBins_0_0_1'])
#pBins_110_0_0_1 = np.array(dataAll['/burg_110/pBins_0_0_1'])
#
#pxBins_100_008_26_22 = np.array(dataAll['/burg_100/pxBins_0.08_2.6_2.2'])
#pzBins_100_008_26_22 = np.array(dataAll['/burg_100/pzBins_0.08_2.6_2.2'])
#pBins_100_008_26_22 = np.array(dataAll['/burg_100/pBins_0.08_2.6_2.2'])
#
#pxBins_110_008_26_22 = np.array(dataAll['/burg_110/pxBins_0.08_2.6_2.2'])
#pzBins_110_008_26_22 = np.array(dataAll['/burg_110/pzBins_0.08_2.6_2.2'])
#pBins_110_008_26_22 = np.array(dataAll['/burg_110/pBins_0.08_2.6_2.2'])

dataAll.close()



pRange=[min(pLength_110_no.min(),pLength_110_1_0_0.min(),pLength_110_0_1_0.min(),pLength_110_0_0_1.min(),pLength_110_008_26_22.min()),
        max(pLength_110_no.max(),pLength_110_1_0_0.max(),pLength_110_0_1_0.max(),pLength_110_0_0_1.max(),pLength_110_008_26_22.max())]

#%% sub figure a,b,c,d the polarization for f11,f12,f44 =1, and 0.08, 2.6 2.2 case range from [-5e10,5e10]
cmapGlob = 'viridis'


scale = pRange[1]*15

#scale =1e11
#heatQuiverPlot_noEdge(pLength_100_no,px_100_no,pz_100_no,pRange,5,scale,params['imgDir']+'/figure_40_p100_no_flexo.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,disColor='red',artists=patch)

times = 1
patch = patch + [ Text(23, 23,'x'+str(times),va='top',ha='right',zorder=99,color='white',fontsize=30)]
patch = []
heatQuiverPlot_noEdge(pLength_110_no,px_110_no,pz_110_no,pRange,5,scale/times,params['imgDir']+'/figure_4a_p110_no_flexo.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,disAngle=45,disColor='red',artists=patch)

times = 4
patch = [ Text(23, 23,'x'+str(times),va='top',ha='right',zorder=99,color='white',fontsize=30)]
#patch = []
heatQuiverPlot_noEdge(pLength_110_1_0_0,px_110_1_0_0,pz_110_1_0_0,pRange,5,scale/times,params['imgDir']+'/figure_4b_p110_1_0_0_flexo.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,disColor='red',disAngle=45,artists=patch)

times = 4
patch = [ Text(23, 23,'x'+str(times),va='top',ha='right',zorder=99,color='white',fontsize=30)]
#patch = []
heatQuiverPlot_noEdge(pLength_110_0_1_0,px_110_0_1_0,pz_110_0_1_0,pRange,5,scale/times,params['imgDir']+'/figure_4c_p110_0_1_0_flexo.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,disColor='red',disAngle=45,artists=patch)

times = 2
patch = [ Text(23, 23,'x'+str(times),va='top',ha='right',zorder=99,color='white',fontsize=30)]
#patch = []
heatQuiverPlot_noEdge(pLength_110_0_0_1,px_110_0_0_1,pz_110_0_0_1,pRange,5,scale/times,params['imgDir']+'/figure_4d_p110_0_0_1_flexo.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,disColor='red',disAngle=45,artists=patch)

patch = []
heatQuiverPlot_noEdge(pLength_110_008_26_22,px_110_008_26_22,pz_110_008_26_22,pRange,5,scale,params['imgDir']+'/figure_4e_p110_008_26_22_flexo.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,disColor='red',disAngle=45,artists=patch)
plotColorBar(params['imgDir']+'/figure_4_p110_ColorBar.svg','Polarization (C/m^2)',[a for a in pRange],5.0,0.7,0.0,1.3,20,'%+.2f','',cmapGlob)

#%% sub figure f






#angles = np.linspace(-175,175,36)
#
#fig = plt.figure(figsize=(5,5))
#legendFig = plt.figure(figsize=(2,5))
#ax1 = fig.add_axes((0.1,0.0,0.9,0.3))
#ax1.plot(angles,p_100_no_bins,marker='o',c='r',label='no')
#ax1.plot(angles,p_100_1_0_0_bins,marker='o',c='y',label='100')
#ax1.plot(angles,p_100_0_1_0_bins,marker='o',c='b',label='010')
#ax1.plot(angles,p_100_0_0_1_bins,marker='o',c='g',label='001')
#ax1.plot(angles,p_100_008_26_22_bins,marker='o',c='c',label='862')
#ax1.set_ylim([0,6])
#ax1.set_xlim([-180,180])
#ax1.set_xticks(np.arange(-180,181,45))
#ax1.set_yticks(np.arange(0,4.1,2.0))
#ax1.grid(True)
#
##ax1.legend()
#
#ax2 = fig.add_axes((0.1,0.3,0.9,0.7))
#ax2.tick_params(bottom=False,length=0,)
#ax2.set_xticks(np.arange(-180,181,45))
#ax2.set_xticklabels([])
#ax2.plot(angles,p_110_no_bins,marker='o',c='r',label='no')
#ax2.plot(angles,p_110_1_0_0_bins,marker='o',c='y',label='100')
#ax2.plot(angles,p_110_0_1_0_bins,marker='o',c='b',label='010')
#ax2.plot(angles,p_110_0_0_1_bins,marker='o',c='g',label='001')
#ax2.plot(angles,p_110_008_26_22_bins,marker='o',c='c',label='862')
##ax2.legend()
#ax2.set_ylim([0,14])
#ax2.set_xlim([-180,180])
#ax2.set_yticks(np.arange(0,12.1,2.0))
#
#ax2.grid(True)
#
##ax.set_rorigin(0.05)
#plt.show()
#
#plt.close()

#%%

angles = np.linspace(-175,175,36)

fig = plt.figure(figsize=(6,5))

figWidth = 0.7
figHeight = 0.18
c1 = '#E1E418'
c1 = '#25AB88'
c2 = '#450559'
c2 = '#3C4F8A'
fs=20
textLocX = 1.07
axisX = 0.1
ax1 = fig.add_axes((axisX,0.0,figWidth,figHeight))
ax11 = ax1.twinx()
ax1.plot(angles,p_100_no_bins,marker='o',c=c1,label='no')
ax11.plot(angles,p_110_no_bins,marker='o',c=c2,label='no')
ax1.text(textLocX,0.5,'No flexo', va='center',ha='left',size=fs,transform=ax1.transAxes)
#ax1.set_ylim([0,2])
#ax1.set_xlim([-180,180])
#ax1.set_yticks(np.arange(1,2.1,1.0))
#ax11.set_ylim([0,2])
#ax11.set_xlim([-180,180])
#ax11.set_yticks(np.arange(1,2.1,1.0))


ax2 = fig.add_axes((axisX,figHeight,figWidth,figHeight))
ax22 = ax2.twinx()
ax2.plot(angles,p_100_1_0_0_bins,marker='o',c=c1,label='100')
ax22.plot(angles,p_110_1_0_0_bins,marker='o',c=c2,label='100')
ax2.text(textLocX,0.5,r'$V_{1111}=1V$', va='center',ha='left',size=fs,transform=ax2.transAxes)
#ax2.set_ylim([0,4])
#ax2.set_xlim([-180,180])
#ax2.set_yticks(np.arange(1,3.1,2.0))
#ax22.set_ylim([0,2])
#ax22.set_xlim([-180,180])
#ax22.set_yticks(np.arange(1,2.1,1.0))


ax3 = fig.add_axes((axisX,figHeight*2,figWidth,figHeight))
ax33 = ax3.twinx()
ax3.plot(angles,p_100_0_1_0_bins,marker='o',c=c1,label='010')
ax33.plot(angles,p_110_0_1_0_bins,marker='o',c=c2,label='010')
ax3.text(textLocX,0.5,r'$V_{1122}=1V$', va='center',ha='left',size=fs,transform=ax3.transAxes)
#ax3.set_ylim([0,4])
#ax3.set_xlim([-180,180])
#ax3.set_yticks(np.arange(1,3.1,2.0))
#ax33.set_ylim([0,2])
#ax33.set_xlim([-180,180])
#ax33.set_yticks(np.arange(1,2.1,1.0))


ax4 = fig.add_axes((axisX,figHeight*3,figWidth,figHeight))
ax44 = ax4.twinx()
ax4.plot(angles,p_100_0_0_1_bins,marker='o',c=c1,label='001')
ax44.plot(angles,p_110_0_0_1_bins,marker='o',c=c2,label='001')
ax4.text(textLocX,0.5,r'$V_{1212}=1V$', va='center',ha='left',size=fs,transform=ax4.transAxes)

ax5 = fig.add_axes((axisX,figHeight*4,figWidth,figHeight))
ax55 = ax5.twinx()
ax5.plot(angles,p_100_008_26_22_bins,marker='o',c=c1,label='862')
ax55.plot(angles,p_110_008_26_22_bins,marker='o',c=c2,label='862')
#ax5.text(textLocX,0.2,'f11=0.08V', va='center',ha='left',size=fs,transform=ax5.transAxes)
ax5.text(textLocX,0.5,'exp. flexo', va='center',ha='left',size=fs,transform=ax5.transAxes)
#ax5.text(textLocX,0.8,'f44=2.2V', va='center',ha='left',size=fs,transform=ax5.transAxes)

ax5.annotate('b=(100)',color=c1,xytext=(0.1,1.2),xy=(0,1.2), arrowprops=dict(edgecolor=c1,facecolor=c1, shrink=0.05),va='center',ha='left',size=fs,xycoords=ax5.transAxes,textcoords=ax5.transAxes)
ax5.annotate('b=(110)',color=c2,xytext=(0.9,1.2),xy=(1,1.2), arrowprops=dict(edgecolor=c2,facecolor=c2, shrink=0.05),va='center',ha='right',size=fs,xycoords=ax5.transAxes,textcoords=ax5.transAxes)

minor_locatorX = AutoMinorLocator(2)
minor_locatorY = AutoMinorLocator(2)

#ax1.add_artist(Line2D([0,45],[1.5,1.5],linewidth=3,color='blue'))
#path_data = [
#    (Path.MOVETO, (5,3)),
#    (Path.LINETO, (5, 1)),
#    (Path.LINETO, (15, 1)),
#    (Path.LINETO, (15, 3)),
#    ]
#codes, verts = zip(*path_data)
#path = Path(verts, codes)
#patch = PathPatch(path, facecolor='None',edgecolor='red', alpha=0.5,lw=2)


for ax in [ax1,ax2,ax3,ax4]:
    ax.set_ylim([0,2])
    ax.set_xlim([-180,180])
    ax.set_yticks(np.arange(1,2.1,1.0))
    ax.set_xticks(np.arange(-180,181,90))
    ax.grid(which='both',zorder=0)
    ax.tick_params(bottom=False,length=0)
    ax.tick_params(labelsize=20)
    ax.set_xticks(np.arange(-180,181,90))
    ax.xaxis.set_minor_locator(minor_locatorX)
    ax.yaxis.set_minor_locator(minor_locatorY)
    ax.set_xticklabels([])
    ax.spines['left'].set_color(c1)
    ax.spines['right'].set_color(c2)
    ax.set_axisbelow(True)
    plt.setp(ax.get_yticklabels(), color=c1)

for ax in [ax11,ax22,ax33,ax44]:
    ax.set_ylim([0,2])
    ax.set_xlim([-180,180])
    ax.set_yticks(np.arange(1,2.1,1.0))
    ax.set_xticks(np.arange(-180,181,90))
    ax.grid(which='both',zorder=0)
    ax.tick_params(bottom=False,length=0)
    ax.tick_params(labelsize=20)
    ax.set_xticks(np.arange(-180,181,90))
    ax.xaxis.set_minor_locator(minor_locatorX)
    ax.yaxis.set_minor_locator(minor_locatorY)
    ax.set_xticklabels([])
    ax.spines['left'].set_color(c1)
    ax.spines['right'].set_color(c2)
    ax.set_axisbelow(True)
    plt.setp(ax.get_yticklabels(), color=c2)
    
for ax in [ax5,ax55]:
    ax.set_ylim([0,4])
    ax.set_xlim([-180,180])
    ax.set_xticks(np.arange(-180,181,90))
    ax.set_yticks(np.arange(1,3.1,2.0))
    ax.grid(which='both',zorder=0)
    ax.tick_params(labelsize=20)
    ax.tick_params(bottom=False,length=0)
    ax.set_xticks(np.arange(-180,181,90))
    ax.xaxis.set_minor_locator(minor_locatorX)
    ax.yaxis.set_minor_locator(minor_locatorY)
    ax.set_xticklabels([])
    ax.spines['left'].set_color(c1)
    ax.spines['right'].set_color(c2)
    ax.set_axisbelow(True)
    plt.setp(ax.get_yticklabels(), color=c1)


for ax in [ax55]:
    ax.set_ylim([0,4])
    ax.set_xlim([-180,180])
    ax.set_xticks(np.arange(-180,181,90))
#    for tick in ax.get_major_ticks():
#        tick.gridline(color='red')
    ax.set_yticks(np.arange(1,3.1,2.0))
    ax.grid(which='both',zorder=0)
#    ax.grid(which='major',zorder=1,axis='x',color='green')
#    ax.grid(which='minor',zorder=1,axis='x',color='blue')
    ax.tick_params(labelsize=20)
    ax.tick_params(bottom=False,length=0)
#    ax.set_xticks(np.arange(-180,181,90))
#    ax.xaxis.set_minor_locator(minor_locatorX)
    ax.yaxis.set_minor_locator(minor_locatorY)
#    ax.set_xticklabels([])
    ax.spines['left'].set_color(c1)
    ax.spines['right'].set_color(c2)
#    ax.set_axisbelow(True)
    plt.setp(ax.get_yticklabels(), color=c2)
    
#for ax in [ax55]:
#    a= ax.get_xgridlines()
#    a[1].set_color('red')
#    a[2].set_color('red')
#    a[3].set_color('red')
    
ax1.set_xticklabels([r'$-180\degree$',r'$-90\degree$',r'$0\degree$',r'$90\degree$',r'$180\degree$'])    
ax1.tick_params(axis='x',labelsize=fs)

ax3.text(textLocX+0.35,0.5,r'$\Sigma |p|(C/m^2)$', va='center',ha='left',size=fs,transform=ax3.transAxes,rotation=90)

#ax1.tick_params(bottom=True,length=5)
#ax1.spines['left'].set_color(c2)
#ax11.spines['left'].set_color(c2)
#mpl.rcParams['axes.edgecolor'] = 'black'

#ax.set_rorigin(0.05)
plt.show()
fig.savefig(params['imgDir']+'/figure_4f_stat.svg')
plt.close()



#%% sub figure f the legend
fig = plt.figure(figsize=(1.5,5))
ax = fig.add_axes((0,0,1.0,1.0))
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.tick_params(length=0)
ax.set_axis_off()
colors = ["crimson", "purple", "gold", "green", "black"]
f = lambda m,c: plt.plot([],[],marker=m, color=c,ls='-')[0]
handles = [f("o", colors[i]) for i in range(5)]
labels = colors
legend = ax.legend(handles, labels, fontsize = 20, loc=6, framealpha=1, frameon=False)

def export_legend(legend, filename="legend.png"):
    fig  = legend.figure
    fig.canvas.draw()
    bbox  = legend.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    fig.savefig(filename, dpi="figure", bbox_inches=bbox)

#export_legend(legend)
plt.show()

#%% sub figure g

#ax.scatter(angles,p_110_no_bins,marker='o',c='r',label='no')
#ax.scatter(angles,p_110_1_0_0_bins,marker='o',c='y',label='100')
#ax.scatter(angles,p_110_0_1_0_bins,marker='o',c='b',label='010')
#ax.scatter(angles,p_110_0_0_1_bins,marker='o',c='g',label='001')
#ax.scatter(angles,p_110_008_26_22_bins,marker='o',c='c',label='862')
#ax.plot(angles,p_110_no_bins,marker='o',c='r',label='no')
#ax.plot(angles,p_110_1_0_0_bins,marker='o',c='y',label='100')
#ax.plot(angles,p_110_0_1_0_bins,marker='o',c='b',label='010')
#ax.plot(angles,p_110_0_0_1_bins,marker='o',c='g',label='001')
#ax.plot(angles,p_110_008_26_22_bins,marker='o',c='c',label='862')
#ax.legend()
#ax.set_rorigin(0.1)
#plt.show()
#plt.close()
#%% sub figure f
#indices = [0.0,0.1,0.2,0.3]
#mi=19
#ma=101
#axWidth = 0.3
#axHeight = 0.18
#fig = plt.figure(figsize=(15,5))
#
#ax1 = fig.add_axes((0,0.1,0.3,0.7))
#ax1.set_xlabel('px',fontsize=30)
#ax1.tick_params(labelsize=20)
#ax1.set_xticks(indices)
##index,pxBins_100_no_avg,pxBins_100_no_std = splitData(pxBins_100_no,mi,ma)
##ax1.plot(index,pxBins_100_no_avg)
##ax1.fill_between(index,pxBins_100_no_avg-pxBins_100_no_std,pxBins_100_no_avg+pxBins_100_no_std,facecolor='blue',alpha=0.5)
#ax1 = addOneLine(ax1,pxBins_100_no,mi,ma)
#
##index,pxBins_110_no_avg,pxBins_110_no_std = splitData(pxBins_110_no,mi,ma)
##ax1.plot(index,pxBins_110_no_avg)
##ax1.fill_between(index,pxBins_110_no_avg-pxBins_110_no_std,pxBins_110_no_avg+pxBins_110_no_std,facecolor='blue',alpha=0.5)
#ax1 = addOneLine(ax1,pxBins_110_no,mi,ma)
#ax1 = addOneLine(ax1,pxBins_100_1_0_0,mi,ma)
#ax1 = addOneLine(ax1,pxBins_110_1_0_0,mi,ma)
#
##ax1.plot(px_100_no[0][mi:ma],px_100_no[1][mi:ma])
##ax1.plot(px_110_no[0][mi:ma],px_110_no[1][mi:ma])
##ax1.plot(px_100_1_0_0[0][mi:ma],px_100_1_0_0[1][mi:ma])
##ax1.plot(px_110_1_0_0[0][mi:ma],px_110_1_0_0[1][mi:ma])
##ax1.plot(px_100_0_1_0[0][mi:ma],px_100_0_1_0[1][mi:ma])
##ax1.plot(px_110_0_1_0[0][mi:ma],px_110_0_1_0[1][mi:ma])
##ax1.plot(px_100_0_0_1[0][mi:ma],px_100_0_0_1[1][mi:ma])
##ax1.plot(px_110_0_0_1[0][mi:ma],px_110_0_0_1[1][mi:ma])
##ax1.plot(px_100_008_26_22[0][mi:ma],px_100_008_26_22[1][mi:ma])
##ax1.plot(px_110_008_26_22[0][mi:ma],px_110_008_26_22[1][mi:ma])
#
#ax2 = fig.add_axes((0.3,0.1,0.3,0.7),yticklabels=[])
#ax2.tick_params(left=False,labelsize=20)
#ax2.set_xticks(indices)
#ax2.set_xlabel('pz',fontsize=30)
##ax2.plot(pz_100_no[0][mi:ma],pz_100_no[1][mi:ma])
##ax2.plot(pz_110_no[0][mi:ma],pz_110_no[1][mi:ma])
##ax2.plot(pz_100_1_0_0[0][mi:ma],pz_100_1_0_0[1][mi:ma])
##ax2.plot(pz_110_1_0_0[0][mi:ma],pz_110_1_0_0[1][mi:ma])
##ax2.plot(pz_100_0_1_0[0][mi:ma],px_100_0_1_0[1][mi:ma])
##ax2.plot(pz_110_0_1_0[0][mi:ma],pz_110_0_1_0[1][mi:ma])
##ax2.plot(pz_100_0_0_1[0][mi:ma],pz_100_0_0_1[1][mi:ma])
##ax2.plot(pz_110_0_0_1[0][mi:ma],pz_110_0_0_1[1][mi:ma])
##ax2.plot(pz_100_008_26_22[0][mi:ma],pz_100_008_26_22[1][mi:ma])
##ax2.plot(pz_110_008_26_22[0][mi:ma],pz_110_008_26_22[1][mi:ma])
#
#ax3 = fig.add_axes((0.6,0.1,0.3,0.7),yticklabels=[])
#ax3.tick_params(left=False,labelsize=20)
#ax3.set_xlabel('p total',fontsize=30)
#ax3.set_xticks(indices)
##ax3.plot(p_100_no[0][mi:ma],p_100_no[1][mi:ma])
##ax3.plot(p_110_no[0][mi:ma],p_110_no[1][mi:ma])
##ax3.plot(p_100_1_0_0[0][mi:ma],p_100_1_0_0[1][mi:ma])
##ax3.plot(p_110_1_0_0[0][mi:ma],p_110_1_0_0[1][mi:ma])
##ax3.plot(p_100_0_1_0[0][mi:ma],p_100_0_1_0[1][mi:ma])
##ax3.plot(p_110_0_1_0[0][mi:ma],p_110_0_1_0[1][mi:ma])
##ax3.plot(p_100_0_0_1[0][mi:ma],p_100_0_0_1[1][mi:ma])
##ax3.plot(p_110_0_0_1[0][mi:ma],p_110_0_0_1[1][mi:ma])
##ax3.plot(p_100_008_26_22[0][mi:ma],p_100_008_26_22[1][mi:ma])
##ax3.plot(p_110_008_26_22[0][mi:ma],p_110_008_26_22[1][mi:ma])
#
#plt.show()
#plt.close()



#%% sub figure e the statistics 


#%%
texEquation(r'no flexo',2.5,0.4,params['imgDir']+'/no_flexo.svg',15)
texEquation(r'$V_{1111}=1V$',2.5,0.4,params['imgDir']+'/f11.svg',15)
texEquation(r'$V_{1122}=1V$',2.5,0.4,params['imgDir']+'/f12.svg',15)
texEquation(r'$V_{1212}=1V$',2.5,0.4,params['imgDir']+'/f44.svg',15)
texEquation(r'exp. flexo',2.5,0.4,params['imgDir']+'/flexoAll.svg',15)
#texEquation(r'f11=0.08,f12=2.6,f44=2.2',2.5,0.4,params['imgDir']+'/flexoAll.svg',15)
#%%



#%% Combine the individual figure files
#figures=[[params['ImgDir']+'figure_1a_stress11.svg',params['ImgDir']+'figure_1a_stress33.svg',params['ImgDir']+'figure_1a_stress13.svg',params['ImgDir']+'figure_1-stressColorBar.svg'],
#         [params['ImgDir']+'figure_1d_sGrad11.svg',params['ImgDir']+'figure_1d_sGrad33.svg',params['ImgDir']+'figure_1d_sGrad33.svg',params['ImgDir']+'figure_1-stressGradientColorBar.svg']]
figureName=[['figure_4a_p110_no_flexo.svg','figure_4e_p110_008_26_22_flexo.svg','figure_4f_stat.svg',''],
            ['no_flexo.svg','flexoAll.svg','',''],
            ['figure_4b_p110_1_0_0_flexo.svg','figure_4c_p110_0_1_0_flexo.svg','figure_4d_p110_0_0_1_flexo.svg','figure_4_p110_ColorBar.svg'],
            ['f11.svg','f12.svg','f44.svg','']]

label = [os.getcwd()+'\\img-new\\'+ name+'.svg' for name in ['a','b','c','d','e','f']]
labelTransparent = [os.getcwd()+'\\img-new\\'+ name+'.svg' for name in ['aa','bb','cc','dd','ee','ff']]

figureWidth = 190
figureHeight= 205
textSize = "20"
textBase = 18
textLeft = 5
boxSize="25"
offset=0
offsetY = 184
labelFont = 'DejaVu Sans'
labelWeight = 'normal'
figures = [[os.getcwd()+params['ImgDir']+figureName[i][j] for j in range(0,4)] for i in range(0,4)]
sc.Figure('17cm','11cm',
          sc.Panel(
                  sc.SVG(figures[0][0]).scale(0.5),
                  sc.SVG(label[0]).move(-1,-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[0][1]).scale(0.5).move(figureWidth,0),
                  sc.SVG(label[1]).move(figureWidth-1,-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[0][2]).scale(0.5).move(figureWidth*2,0),
                  sc.SVG(labelTransparent[2]).move(figureWidth*2-1,0-1)
                  ),
#          sc.Panel(
#                  sc.SVG(figures[0][3]).scale(0.5).move(figureWidth*3,0),
#                  sc.Element(etree.Element("rect",width=boxSize,height=boxSize,fill="rgb(255,255,255)")).move(figureWidth*3,0),
#                  sc.Text('d',figureWidth*3+textLeft,textBase,size=textSize)
#                  ),
          sc.Panel(
                  sc.SVG(figures[1][0]).move(offset+figureWidth*0,offsetY)
                  ),          
          sc.Panel(
                  sc.SVG(figures[1][1]).move(offset+figureWidth*1,offsetY)
                  ),
#          sc.Panel(
#                  sc.SVG(figures[1][2]).move(offset+figureWidth*2,offsetY)
#                  ),
#          sc.Panel(
#                  sc.SVG(figures[1][3]).move(offset+figureWidth*3,offsetY)
#                  ),
          sc.Panel(
                  sc.SVG(figures[2][0]).scale(0.5).move(0,figureHeight),
                  sc.SVG(label[3]).move(0-1,figureHeight-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[2][1]).scale(0.5).move(figureWidth*1,figureHeight),
                  sc.SVG(label[4]).move(figureWidth-1,figureHeight-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[2][2]).scale(0.5).move(figureWidth*2,figureHeight),
                  sc.SVG(label[5]).move(figureWidth*2-1,figureHeight-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[2][3]).scale(0.5).move(figureWidth*3,figureHeight)
                  ),
          sc.Panel(
                  sc.SVG(figures[3][0]).move(offset+figureWidth*0,offsetY+figureHeight)
                  ),     
          sc.Panel(
                  sc.SVG(figures[3][1]).move(offset+figureWidth*1,offsetY+figureHeight)
                  ),
          sc.Panel(
                  sc.SVG(figures[3][2]).move(offset+figureWidth*2,offsetY+figureHeight)
                  )
          ).save(params['imgFile'])
#          sc.Grid(20,20)
#%%
fixSVGASCII(params['imgFile'])
with open(params['captionFile'],'w') as file:
    file.write(params['caption'])