# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 14:17:47 2019

@author: cxx
"""
import importlib
import sys
import os
import platform

if platform.system() == 'Windows':
    os.chdir("D:/SynologyDrive/Research/STO-Dislocation-Flexo/paper")
    newPath = os.getcwd()+'\\code'
elif platform.system() == 'Linux':
    os.chdir('/gpfs/scratch/xuc116/sto-dislocation')
    newPath = os.getcwd()+'/code'
else:
    os.chdir('/Users/xiaoxingcheng/SynologyDrive/Research/STO-Dislocation-Flexo/paper')    
    newPath = os.getcwd()+'/code'

if newPath not in sys.path:
    print("Append new path")
    sys.path.append(newPath)
    
from STODislocationHeader import *
#%%
params={}

params['imgDir']='./img-new/sup5'
params['imgFile']='./img-new/sup5.svg'

params['ImgDir']='\\img-new\\sup5\\'
params['datDir']='/burg_100/'
params['start'] = 2000

params['nx']=512
params['ny']=1
params['nz']=512

params['xrange']=[244,268]
params['yrange']=[114,138]

params['disLocX']=0.5
params['disLocY']=0.5

params['batchKey']=['&MATERIAL.FLEXOCON','&PNOISEED','&QNOISEED']

touch(params['imgDir'])

x1=params['xrange'][0]
x2=params['xrange'][1]
y1=params['yrange'][0]
y2=params['yrange'][1]


params['captionFile'] = './img-new/sup5.md'
params['caption'] = """

## Supplementary 5
![Supplementary 5](sup5.svg)

The polarization and flexoelectric field distribution with different flexoelectric coefficients (case 6, 7, 8) for (100) dislocation.
(a), (b) and (c) polarization distribution for case 6, 7, and 8 respectively, the white quiver represent polarization vector and background heat map shows the polarization magnitude. 
(d), (e) and (f) flexoelectric field distribution for case 6, 7, and 8 respectively, the white quiver shows the field vector, and background heat maps represent the flexoelectric field magnitude. 
The arrows in (d) is enlarged 4 times.
"""

#%% open the file, important always remember to close it
#dataAll= h5py.File('20190325_STO_dislocation_flexo.h5','r')
dataAll= h5py.File('20190422_STO_dislocation_flexo_mech.h5','r')

# f11=1
px_100 = np.array(dataAll[params['datDir']+'11+&MATERIAL.FLEXOCON_2_0_0+&PNOISEED_1+QNOISEED_1/px'][x1:x2,y1:y2])
pz_100 = np.array(dataAll[params['datDir']+'11+&MATERIAL.FLEXOCON_2_0_0+&PNOISEED_1+QNOISEED_1/pz'][x1:x2,y1:y2])
pLength_100 = np.sqrt(np.square(np.array(px_100))+np.square(np.array(pz_100)))

flexoX_100 = np.array(dataAll[params['datDir']+'11+&MATERIAL.FLEXOCON_2_0_0+&PNOISEED_1+QNOISEED_1/flexoField1'][x1:x2,y1:y2])
flexoZ_100 = np.array(dataAll[params['datDir']+'11+&MATERIAL.FLEXOCON_2_0_0+&PNOISEED_1+QNOISEED_1/flexoField3'][x1:x2,y1:y2])
flexoLength_100 = np.sqrt(np.square(np.array(flexoX_100))+np.square(np.array(flexoZ_100)))

# f12=1
px_010 = np.array(dataAll[params['datDir']+'21+&MATERIAL.FLEXOCON_0_2_0+&PNOISEED_1+QNOISEED_1/px'][x1:x2,y1:y2])
pz_010 = np.array(dataAll[params['datDir']+'21+&MATERIAL.FLEXOCON_0_2_0+&PNOISEED_1+QNOISEED_1/pz'][x1:x2,y1:y2])
pLength_010 = np.sqrt(np.square(np.array(px_010))+np.square(np.array(pz_010)))
flexoX_010 = np.array(dataAll[params['datDir']+'21+&MATERIAL.FLEXOCON_0_2_0+&PNOISEED_1+QNOISEED_1/flexoField1'][x1:x2,y1:y2])
flexoZ_010 = np.array(dataAll[params['datDir']+'21+&MATERIAL.FLEXOCON_0_2_0+&PNOISEED_1+QNOISEED_1/flexoField3'][x1:x2,y1:y2])
flexoLength_010 = np.sqrt(np.square(np.array(flexoX_010))+np.square(np.array(flexoZ_010)))

# f44=1
px_001 = np.array(dataAll[params['datDir']+'31+&MATERIAL.FLEXOCON_0_0_2+&PNOISEED_1+QNOISEED_1/px'][x1:x2,y1:y2])
pz_001 = np.array(dataAll[params['datDir']+'31+&MATERIAL.FLEXOCON_0_0_2+&PNOISEED_1+QNOISEED_1/pz'][x1:x2,y1:y2])
pLength_001 = np.sqrt(np.square(np.array(px_001))+np.square(np.array(pz_001)))
flexoX_001 = np.array(dataAll[params['datDir']+'31+&MATERIAL.FLEXOCON_0_0_2+&PNOISEED_1+QNOISEED_1/flexoField1'][x1:x2,y1:y2])
flexoZ_001 = np.array(dataAll[params['datDir']+'31+&MATERIAL.FLEXOCON_0_0_2+&PNOISEED_1+QNOISEED_1/flexoField3'][x1:x2,y1:y2])
flexoLength_001 = np.sqrt(np.square(np.array(flexoX_001))+np.square(np.array(flexoZ_001)))


dataAll.close()

pRange=[min(pLength_100.min(),pLength_010.min(),pLength_001.min()),max(pLength_100.max(),pLength_010.max(),pLength_001.max())]
flexoRange = [min(flexoLength_100.min(),flexoLength_010.min(),flexoLength_001.min()),max(flexoLength_100.max(),flexoLength_010.max(),flexoLength_001.max())]




#%% sub figure a,b,c the polarization for f11,f12,f44 =1, range from [-5e10,5e10]
cmapGlob = 'viridis'


scale = pRange[1]*15

#scale =1e11
patch = getScaleBar(4,1,5,1.5,'2nm',color='white',lw=5,fs=30,padding=[1,1],bg = 'None')
heatQuiverPlot_noEdge(pLength_100,px_100,pz_100,pRange,5,scale,params['imgDir']+'/figure_3a_f11=1_p.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,disColor='red',artists=patch)
heatQuiverPlot_noEdge(pLength_010,px_010,pz_010,pRange,5,scale,params['imgDir']+'/figure_3b_f12=1_p.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,disColor='red')
heatQuiverPlot_noEdge(pLength_001,px_001,pz_001,pRange,5,scale,params['imgDir']+'/figure_3c_f44=1_p.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,disColor='red')
plotColorBar(params['imgDir']+'/figure_3_p_ColorBar.svg','Polarization (C/m^2)',[a for a in pRange],5.0,0.7,0.0,1.3,20,'%+.1f','',cmapGlob)

#%% sub figure d,e,f the stress gradient, range from []
cmapGlob = 'inferno'


scale = flexoRange[1]*15

#scale =1e11
times = 4
patch = [ Text(23, 23,'x'+str(times),va='top',ha='right',zorder=99,color='white',fontsize=30)]
heatQuiverPlot_noEdge(flexoLength_100,flexoX_100,flexoZ_100,flexoRange,5,scale/times,params['imgDir']+'/figure_3d_f11=1_flexo.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,artists=patch)
heatQuiverPlot_noEdge(flexoLength_010,flexoX_010,flexoZ_010,flexoRange,5,scale,params['imgDir']+'/figure_3e_f12=1_flexo.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob)
heatQuiverPlot_noEdge(flexoLength_001,flexoX_001,flexoZ_001,flexoRange,5,scale,params['imgDir']+'/figure_3f_f44=1_flexo.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob)
plotColorBar(params['imgDir']+'/figure_3-flexo_ColorBar.svg','Flexo field (V/m)',[a/1.0e8 for a in flexoRange],4.6,0.7,0.4,1.3,20,'%+.1f','1e8',cmapGlob)

#%%
texEquation(r'$V_{1111}=2V,\; \mathrm{P}$',2.5,0.4,params['imgDir']+'/f11_p.svg',15)
texEquation(r'$V_{1122}=2V,\; \mathrm{P}$',2.5,0.4,params['imgDir']+'/f12_p.svg',15)
texEquation(r'$V_{1212}=2V,\; \mathrm{P}$',2.5,0.4,params['imgDir']+'/f44_p.svg',15)
texEquation(r'$V_{1111}=2V,\; \mathrm{flexo\; E}$',2.5,0.4,params['imgDir']+'/f11_flexo.svg',15)
texEquation(r'$V_{1122}=2V,\; \mathrm{flexo\; E}$',2.5,0.4,params['imgDir']+'/f12_flexo.svg',15)
texEquation(r'$V_{1212}=2V,\; \mathrm{flexo\; E}$',2.5,0.4,params['imgDir']+'/f44_flexo.svg',15)
#%%



#%% Combine the individual figure files
#figures=[[params['ImgDir']+'figure_1a_stress11.svg',params['ImgDir']+'figure_1a_stress33.svg',params['ImgDir']+'figure_1a_stress13.svg',params['ImgDir']+'figure_1-stressColorBar.svg'],
#         [params['ImgDir']+'figure_1d_sGrad11.svg',params['ImgDir']+'figure_1d_sGrad33.svg',params['ImgDir']+'figure_1d_sGrad33.svg',params['ImgDir']+'figure_1-stressGradientColorBar.svg']]
figureName=[['figure_3a_f11=1_p.svg','figure_3b_f12=1_p.svg','figure_3c_f44=1_p.svg','figure_3_p_ColorBar.svg'],
            ['f11_p.svg','f12_p.svg','f44_p.svg',''],
            ['figure_3d_f11=1_flexo.svg','figure_3e_f12=1_flexo.svg','figure_3f_f44=1_flexo.svg','figure_3-flexo_ColorBar.svg'],
            ['f11_flexo.svg','f12_flexo.svg','f44_flexo.svg','']]

label = [os.getcwd()+'\\img-new\\'+ name+'.svg' for name in ['a','b','c','d','e','f']]
labelTransparent = [os.getcwd()+'\\img-new\\'+ name+'.svg' for name in ['aa','bb','cc','dd','ee','ff']]

figureWidth = 190
figureHeight = 205
textSize = "20"
textBase = 18
textLeft = 5
boxSize="25"
offset=0
offsetY = 184
labelFont = 'DejaVu Sans'
labelWeight = 'normal'
figures = [[os.getcwd()+params['ImgDir']+figureName[i][j] for j in range(0,4)] for i in range(0,4)]
sc.Figure('17cm','11cm',
          sc.Panel(
                  sc.SVG(figures[0][0]).scale(0.5),
                  sc.SVG(label[0]).move(-1,-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[0][1]).scale(0.5).move(figureWidth,0),
                  sc.SVG(label[1]).move(figureWidth-1,0-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[0][2]).scale(0.5).move(figureWidth*2,0),
                  sc.SVG(label[2]).move(figureWidth*2-1,0-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[0][3]).scale(0.5).move(figureWidth*3,0)
                  ),
          sc.Panel(
                  sc.SVG(figures[1][0]).move(offset+figureWidth*0,offsetY)
                  ),          
          sc.Panel(
                  sc.SVG(figures[1][1]).move(offset+figureWidth*1,offsetY)
                  ),
          sc.Panel(
                  sc.SVG(figures[1][2]).move(offset+figureWidth*2,offsetY)
                  ),
          sc.Panel(
                  sc.SVG(figures[2][0]).scale(0.5).move(0,figureHeight),
                  sc.SVG(label[3]).move(0-1,figureHeight-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[2][1]).scale(0.5).move(figureWidth,figureHeight),
                  sc.SVG(label[4]).move(figureWidth-1,figureHeight-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[2][2]).scale(0.5).move(figureWidth*2,figureWidth+15),
                  sc.SVG(label[5]).move(figureWidth*2-1,figureHeight-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[2][3]).scale(0.5).move(figureWidth*3,figureHeight)
                  ),
          sc.Panel(
                  sc.SVG(figures[3][0]).move(offset+figureWidth*0,offsetY+figureHeight)
                  ),          
          sc.Panel(
                  sc.SVG(figures[3][1]).move(offset+figureWidth*1,offsetY+figureHeight)
                  ),
          sc.Panel(
                  sc.SVG(figures[3][2]).move(offset+figureWidth*2,offsetY+figureHeight)
                  )
          ).save(params['imgFile'])
#          sc.Grid(20,20)
#%%
fixSVGASCII(params['imgFile'])          

with open(params['captionFile'],'w') as file:
    file.write(params['caption'])


