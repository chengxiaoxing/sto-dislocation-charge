# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 14:17:47 2019

@author: cxx
"""
import importlib
import sys
import os
import platform

if platform.system() == 'Windows':
    os.chdir("D:/SynologyDrive/Research/STO-Dislocation-Flexo/paper")
    newPath = os.getcwd()+'\\code'
elif platform.system() == 'Linux':
    os.chdir('/gpfs/scratch/xuc116/sto-dislocation')
    newPath = os.getcwd()+'/code'
else:
    os.chdir('/Users/xiaoxingcheng/SynologyDrive/Research/STO-Dislocation-Flexo/paper')    
    newPath = os.getcwd()+'/code'

if newPath not in sys.path:
    print("Append new path")
    sys.path.append(newPath)
    
from STODislocationHeader import *

#%%
params={}

params['imgDir']='./img-new/sup8'
params['imgFile'] = './img-new/sup8.svg'
params['ImgDir']='\\img-new\\sup8\\'
params['datDir']='/burg_100/01+&MATERIAL.FLEXOCON_0_0_0+&PNOISEED_1+QNOISEED_1/'
params['datDir1']='/burg_110/01+&MATERIAL.FLEXOCON_0_0_0+&PNOISEED_1+QNOISEED_1/'
params['curDir']="D:/SynologyDrive/Research/STO-Dislocation-Flexo/paper"
params['start'] = 2000
params['polar'] = 'Polar.00002000.dat'
params['stress'] = 'Stress.00002001.dat'
params['flexo'] = 'Eflexo.00002001.dat'

params['nx']=512
params['ny']=1
params['nz']=512

params['xrange']=[244,268]
params['yrange']=[114,138]

params['disLocX']=0.5
params['disLocY']=0.5

params['Xrange']=[115,139]
params['Yrange']=[115,139]

params['batchKey']=['&MATERIAL.FLEXOCON','&PNOISEED','&QNOISEED']

touch(params['imgDir'])
x1=params['xrange'][0]
x2=params['xrange'][1]
y1=params['yrange'][0]
y2=params['yrange'][1]

x3=params['Xrange'][0]
x4=params['Xrange'][1]
y3=params['Yrange'][0]
y4=params['Yrange'][1]
#%% open the file, important always remember to close it
#dataAll= h5py.File('20190325_STO_dislocation_flexo.h5','r')
# be careful with the gradient, since we store the data in x,y,z order, but when plotting, the 2D array is mapped directly to the image, 
# the x and y axis needs to be reversed, so the gradient we calculated using the original data needs to be switched when used.
dataAll= h5py.File('20190422_STO_dislocation_flexo_mech.h5','r')


density = 1

# a s11
str11 = np.array(dataAll[params['datDir']+'elasticStrain11'][x1:x2,y1:y2])
# b s33
str33 = np.array(dataAll[params['datDir']+'elasticStrain33'][x1:x2,y1:y2])
# c s13
str13 = np.array(dataAll[params['datDir']+'elasticStrain13'][x1:x2,y1:y2])

e11gY,e11gX = np.gradient(str11,edge_order=1)
e11gLength = np.sqrt(np.square(np.array(e11gX))+np.square(np.array(e11gY)))

e33gY,e33gX = np.gradient(str33,edge_order=1)
e33gLength = np.sqrt(np.square(np.array(e33gX))+np.square(np.array(e33gY)))

e13gY,e13gX = np.gradient(str13,edge_order=1)
e13gLength = np.sqrt(np.square(np.array(e13gX))+np.square(np.array(e13gY)))

# a s11
str11_110 = np.array(dataAll[params['datDir1']+'elasticStrain11'][x3:x4,y3:y4])
# b s33
str33_110 = np.array(dataAll[params['datDir1']+'elasticStrain33'][x3:x4,y3:y4])
# c s13
str13_110 = np.array(dataAll[params['datDir1']+'elasticStrain13'][x3:x4,y3:y4])

e11gY_110,e11gX_110 = np.gradient(str11_110,edge_order=1)
e11gLength_110 = np.sqrt(np.square(np.array(e11gX_110))+np.square(np.array(e11gY_110)))

e33gY_110,e33gX_110 = np.gradient(str33_110,edge_order=1)
e33gLength_110 = np.sqrt(np.square(np.array(e33gX_110))+np.square(np.array(e33gY_110)))

e13gY_110,e13gX_110 = np.gradient(str13_110,edge_order=1)
e13gLength_110 = np.sqrt(np.square(np.array(e13gX_110))+np.square(np.array(e13gY_110)))



dataAll.close()

strainRange=[min(str11.min(),str33.min(),str13.min()),max(str11.max(),str33.max(),str13.max())]
strainRange_110=[min(str11_110.min(),str33_110.min(),str13_110.min()),max(str11_110.max(),str33_110.max(),str13_110.max())]
gradientStrainRange     = [min(e11gLength.min(),e33gLength.min(),e13gLength.min()),max(e11gLength.max(),e33gLength.max(),e13gLength.max())]
gradientStrainRange_110 = [min(e11gLength_110.min(),e33gLength_110.min(),e13gLength_110.min()),max(e11gLength_110.max(),e33gLength_110.max(),e13gLength_110.max())]



#%% sub figure a,b,c the stress 11, 33, 13, range from [-5e10,5e10]
cmapGlob = 'inferno'


patch = getScaleBar(4,1,5,1.5,'2nm',color='white',lw=5,fs=30,padding=[1,1],bg = 'None')
heatPlot_noEdge(str11,strainRange,5,params['imgDir']+'/figure_sup8a_strain11.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,artists=patch)
heatPlot_noEdge(str33,strainRange,5,params['imgDir']+'/figure_sup8b_strain33.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob)
heatPlot_noEdge(str13,strainRange,5,params['imgDir']+'/figure_sup8c_strain13.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob)
plotColorBar(params['imgDir']+'/figure_sup8-strainColorBar.svg','Strain',[a/1e-2 for a in strainRange],4.6,0.7,0.4,1.3,20,'%+0.1f','1e-2',cmapGlob=cmapGlob)

#%% sub figure d,e,f the stress gradient, range from []
cmapGlob = 'inferno'



#scale =1e11
heatPlot_noEdge(str11_110,strainRange_110,5,params['imgDir']+'/figure_sup8d_strain11_110.svg',disx=params['disLocX'],disy=params['disLocY'],disAngle=45,cmapGlob=cmapGlob)
heatPlot_noEdge(str33_110,strainRange_110,5,params['imgDir']+'/figure_sup8e_strain33_110.svg',disx=params['disLocX'],disy=params['disLocY'],disAngle=45,cmapGlob=cmapGlob)
heatPlot_noEdge(str13_110,strainRange_110,5,params['imgDir']+'/figure_sup8f_strain13_110.svg',disx=params['disLocX'],disy=params['disLocY'],disAngle=45,cmapGlob=cmapGlob)
plotColorBar(params['imgDir']+'/figure_sup8-strainColorBar_110.svg','Strain',[a/1e-2 for a in strainRange_110],4.6,0.7,0.4,1.3,20,'%+0.1f','1e-2',cmapGlob=cmapGlob)


#%%


#%%
texEquation(r'$\epsilon_{11}$',0.8,0.4,params['imgDir']+'/e11.svg',20)
texEquation(r'$\epsilon_{33}$',0.8,0.4,params['imgDir']+'/e33.svg',20)
texEquation(r'$\epsilon_{13}$',0.8,0.4,params['imgDir']+'/e13.svg',20)
#%%

texEquation(r'a',0.4,0.4,'./img-new/a.svg',20,va='center',ha='center',posX = 0.5,posY=0.5)
texEquation(r'b',0.4,0.4,'./img-new/b.svg',20,va='center',ha='center',posX = 0.5,posY=0.5)
texEquation(r'c',0.4,0.4,'./img-new/c.svg',20,va='center',ha='center',posX = 0.5,posY=0.5)
texEquation(r'd',0.4,0.4,'./img-new/d.svg',20,va='center',ha='center',posX = 0.5,posY=0.5)
texEquation(r'e',0.4,0.4,'./img-new/e.svg',20,va='center',ha='center',posX = 0.5,posY=0.5)
texEquation(r'f',0.4,0.4,'./img-new/f.svg',20,va='center',ha='center',posX = 0.5,posY=0.5)

trans = True
texEquation(r'a',0.4,0.4,'./img-new/aa.svg',20,va='center',ha='center',posX = 0.5,posY=0.5,transparent=trans)
texEquation(r'b',0.4,0.4,'./img-new/bb.svg',20,va='center',ha='center',posX = 0.5,posY=0.5,transparent=trans)
texEquation(r'c',0.4,0.4,'./img-new/cc.svg',20,va='center',ha='center',posX = 0.5,posY=0.5,transparent=trans)
texEquation(r'd',0.4,0.4,'./img-new/dd.svg',20,va='center',ha='center',posX = 0.5,posY=0.5,transparent=trans)
texEquation(r'e',0.4,0.4,'./img-new/ee.svg',20,va='center',ha='center',posX = 0.5,posY=0.5,transparent=trans)
texEquation(r'f',0.4,0.4,'./img-new/ff.svg',20,va='center',ha='center',posX = 0.5,posY=0.5,transparent=trans)


#%% Combine the individual figure files
#figures=[[params['ImgDir']+'figure_1a_stress11.svg',params['ImgDir']+'figure_1a_stress33.svg',params['ImgDir']+'figure_1a_stress13.svg',params['ImgDir']+'figure_1-stressColorBar.svg'],
#         [params['ImgDir']+'figure_1d_sGrad11.svg',params['ImgDir']+'figure_1d_sGrad33.svg',params['ImgDir']+'figure_1d_sGrad33.svg',params['ImgDir']+'figure_1-stressGradientColorBar.svg']]
#figureName=[['figure_1a_stress11.svg','figure_1b_stress33.svg','figure_1c_stress13.svg','figure_1-stressColorBar.svg'],
#            ['sigma11.svg','sigma33.svg','sigma13.svg',''],
#            ['figure_1d_sGrad11.svg','figure_1e_sGrad33.svg','figure_1f_sGrad13.svg','figure_1-stressGradientColorBar.svg'],
#            ['nabla_sigma11.svg','nabla_sigma33.svg','nabla_sigma13.svg','']]

figureName=[['figure_sup8a_strain11.svg','figure_sup8b_strain33.svg','figure_sup8c_strain13.svg','figure_sup8-strainColorBar.svg'],
            ['e11.svg','e33.svg','e13.svg',''],
            ['figure_sup8d_strain11_110.svg','figure_sup8e_strain33_110.svg','figure_sup8f_strain13_110.svg','figure_sup8-strainColorBar_110.svg'],
            ]
figureWidth = 190
figureHeight = 205
textSize = "20"
textBase = 18
textLeft = 5
boxSize="25"
offsetY = 182
figures = [[os.getcwd()+params['ImgDir']+figureName[i][j] for j in range(0,4)] for i in range(0,3)]
label = [os.getcwd()+'\\img-new\\'+ name+'.svg' for name in ['a','b','c','d','e','f']]
labelFont = 'DejaVu Sans'
labelWeight = 'normal'
sc.Figure('17cm','11cm',
          sc.Panel(
                  sc.SVG(figures[0][0]).scale(0.5),
                  sc.SVG(label[0]).move(-1,-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[0][1]).scale(0.5).move(figureWidth,0),
                  sc.SVG(label[1]).move(figureWidth-1,0-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[0][2]).scale(0.5).move(figureWidth*2,0),
                  sc.SVG(label[2]).move(figureWidth*2-1,0-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[0][3]).scale(0.5).move(figureWidth*3,0)
                  ),
          sc.Panel(
                  sc.SVG(figures[1][0]).move(72+figureWidth*0,offsetY)
                  ),          
          sc.Panel(
                  sc.SVG(figures[1][1]).move(72+figureWidth*1,offsetY)
                  ),
          sc.Panel(
                  sc.SVG(figures[1][2]).move(72+figureWidth*2,offsetY)
                  ),
          sc.Panel(
                  sc.SVG(figures[2][0]).scale(0.5).move(0,figureHeight),
                  sc.SVG(label[3]).move(0-1,figureHeight-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[2][1]).scale(0.5).move(figureWidth,figureHeight),
                  sc.SVG(label[4]).move(figureWidth-1,figureHeight-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[2][2]).scale(0.5).move(figureWidth*2,figureHeight),
                  sc.SVG(label[5]).move(figureWidth*2-1,figureHeight-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[2][3]).scale(0.5).move(figureWidth*3,figureHeight)
                  )
          ).save(params['imgFile'])
#          sc.Grid(20,20)
          
#%%
fixSVGASCII(params['imgFile'])