# -*- coding: utf-8 -*-
"""
Created on Wed Mar 27 19:47:09 2019

@author: cxx
"""

import importlib
import sys
import os
import platform

if platform.system() == 'Windows':
    os.chdir("D:/SynologyDrive/Research/STO-Dislocation-Flexo/paper")
    newPath = os.getcwd()+'\\code'
elif platform.system() == 'Linux':
    os.chdir('/gpfs/scratch/xuc116/sto-dislocation')
    newPath = os.getcwd()+'/code'
else:
    os.chdir('/Users/xiaoxingcheng/SynologyDrive/Research/STO-Dislocation-Flexo/paper')    
    newPath = os.getcwd()+'/code'

if newPath not in sys.path:
    print("Append new path")
    sys.path.append(newPath)
    
from STODislocationHeader import *

#%%
def circle(ax,text,centerX,centerY,textShift,radius,fs=20,color='#AFD5AA'):
    rect = Circle((centerX,centerY),radius=radius,facecolor=color,edgecolor='None',zorder=10)
    ax.add_patch(rect)
    ax.text(centerX+textShift[0],centerY+textShift[1],text,horizontalalignment='center',verticalalignment='center',fontsize=fs)

def box(ax,text,centerX,centerY,textShift,width,height,heightColor,fs=20):
    totalHeight = sum(height)
    base = centerY + totalHeight/2.0
    for i in range(0,len(height)):
        print(i,base,height[i])
        base = base - height[i]
        rect = Rectangle((centerX-width/2.0,base),width,height[i],facecolor=heightColor[i],edgecolor='None',zorder=10)
        ax.add_patch(rect)
    ax.text(centerX+textShift[0],centerY+textShift[1],text,horizontalalignment='center',verticalalignment='center',fontsize=fs)
#%%
params={}
params['imgDir']='./img-new/sup4'
params['imgFile']='./img-new/sup4.svg'
params['datDir']='/burg_100/01+&MATERIAL.FLEXOCON_0_0_0+&PNOISEED_1+QNOISEED_1/'

touch(params['imgDir'])

params['xrange']=[244,268]
params['yrange']=[114,138]

x1=params['xrange'][0]
x2=params['xrange'][1]
y1=params['yrange'][0]
y2=params['yrange'][1]

params['captionFile'] = './img-new/sup4.md'
params['caption'] = """

## Supplementary 4
![Supplementary 4](sup4.svg)

Illustration of each strain gradient component's contribution to the final flexoelectric field for case 2, which used the experimental flexoelectric coefficient setup..
The light green column is strain gradient, and the area of each circle is propotional to the maximum value of that strain gradient component.
The dark green column is the flexoelectric coefficient for case 2, the area of which is proportional to the flexoelectric coefficient value.
The two rectangle in the right column represent the x and z component of flexoelectric field vector. The area of each color shows the contribution from different strain gradient.
"""

#%% be careful with the gradient, since we store the data in x,y,z order, but when plotting, the 2D array is mapped directly to the image, 
# the x and y axis needs to be reversed, so the gradient we calculated using the original data needs to be switched when used.
# the normal gradient should be Y first and then X, for here we can use X first and then Y
dataAll= h5py.File('20190422_STO_dislocation_flexo_mech.h5','r')
# a s11
s11 = np.array(dataAll[params['datDir']+'stress11'][x1:x2,y1:y2])
# b s33
s33 = np.array(dataAll[params['datDir']+'stress33'][x1:x2,y1:y2])
# c s13
s13 = np.array(dataAll[params['datDir']+'stress13'][x1:x2,y1:y2])

density = 1

# a s11
str11 = np.array(dataAll[params['datDir']+'elasticStrain11'][x1:x2,y1:y2])
# b s33
str33 = np.array(dataAll[params['datDir']+'elasticStrain33'][x1:x2,y1:y2])
# c s13
str13 = np.array(dataAll[params['datDir']+'elasticStrain13'][x1:x2,y1:y2])

e11gX,e11gY = np.gradient(str11,edge_order=0)
e11gLength = np.sqrt(np.square(np.array(e11gX))+np.square(np.array(e11gY)))

e33gX,e33gY = np.gradient(str33,edge_order=0)
e33gLength = np.sqrt(np.square(np.array(e33gX))+np.square(np.array(e33gY)))

e13gX,e13gY = np.gradient(str13,edge_order=0)
e13gLength = np.sqrt(np.square(np.array(e13gX))+np.square(np.array(e13gY)))

# a s11
s11gX = dataAll[params['datDir']+'stress11GradientX'][x1:x2:density,y1:y2:density]
s11gY = dataAll[params['datDir']+'stress11GradientY'][x1:x2:density,y1:y2:density]
s11gLength = np.sqrt(np.square(np.array(s11gX))+np.square(np.array(s11gY)))

# b s33
s33gX = dataAll[params['datDir']+'stress33GradientX'][x1:x2:density,y1:y2:density]
s33gY = dataAll[params['datDir']+'stress33GradientY'][x1:x2:density,y1:y2:density]
s33gLength = np.sqrt(np.square(np.array(s33gX))+np.square(np.array(s33gY)))
# c s13
s13gX = dataAll[params['datDir']+'stress13GradientX'][x1:x2:density,y1:y2:density]
s13gY = dataAll[params['datDir']+'stress13GradientY'][x1:x2:density,y1:y2:density]
s13gLength = np.sqrt(np.square(np.array(s13gX))+np.square(np.array(s13gY)))

dataAll.close()

#%% supplementary simulation setup illustration
fig = plt.figure(figsize=(8,4))
ax = fig.add_axes([0,0,1,1])


ax.axis('off')

firstCol = 10
secondCol = 130
thirdCol = 250
rows1 = np.linspace(160,0,6)
rows2 = np.linspace(140,20,3)
rows3 = np.linspace(150,30,2)
scale1 = 80
scale2 = 10
scale3 = 1000
scale4 = scale1*2
scale5 = 0.1
textShift1 = [-40,0]
textShift2 = [0,16]
textShift3 = [70,0]
v1111=0.08
v1122=2.6
v1212=2.2
width = 40

coeffColor = '#50614E'
cg1 = []
cg2 = []
cg3 = []

e11gXM = np.abs(e11gX).max()
e11gYM = np.abs(e11gY).max()
e33gXM = np.abs(e33gX).max()
e33gYM = np.abs(e33gY).max()
e13gXM = np.abs(e13gX).max()
e13gYM = np.abs(e13gY).max()

strG=[0]*6
strG[0] = scale1*sqrt(e11gXM)
strG[1] = scale1*sqrt(e11gYM)
strG[2] = scale1*sqrt(e33gXM)
strG[3] = scale1*sqrt(e33gYM)
strG[4] = scale1*sqrt(e13gXM)
strG[5] = scale1*sqrt(e13gYM)

v=[0]*3
v[0] = scale2*sqrt(v1111)
v[1] = scale2*sqrt(v1122)
v[2] = scale2*sqrt(v1212)

#ax.text(firstCol,firstRow+offset,'Flexoelectric \n coefficient (V)',horizontalalignment='center',verticalalignment='center',fontsize = titleSize)
circle(ax,r'$\epsilon_{11,1}$',firstCol,rows1[0],textShift1,strG[0])
circle(ax,r'$\epsilon_{11,3}$',firstCol,rows1[1],textShift1,strG[1])
circle(ax,r'$\epsilon_{33,1}$',firstCol,rows1[2],textShift1,strG[2])
circle(ax,r'$\epsilon_{33,3}$',firstCol,rows1[3],textShift1,strG[3])
circle(ax,r'$\epsilon_{13,1}$',firstCol,rows1[4],textShift1,strG[4])
circle(ax,r'$\epsilon_{13,3}$',firstCol,rows1[5],textShift1,strG[5])

circle(ax,r'$V_{1111}$',secondCol,rows2[0],[textShift2[0],textShift2[1]+v[0]/2],v[0],color=coeffColor)
circle(ax,r'$V_{1122}$',secondCol,rows2[1],[textShift2[0],textShift2[1]+v[1]/2],v[1],color=coeffColor)
circle(ax,r'$V_{1212}$',secondCol,rows2[2],[textShift2[0],textShift2[1]+v[2]/2],v[2],color=coeffColor)

height = [0]*3
height[0]=v1111*e11gXM*scale3
height[1]=v1122*e33gXM*scale3
height[2]=v1212*e13gYM*scale3
heightColor = ['#D18828','#FFB655','#FFD6A0']
box(ax,r'flexo Ex',thirdCol,rows3[0],textShift3,width,height,heightColor)

ax.add_line(Line2D([firstCol,secondCol],[rows1[0],rows2[0]],color=heightColor[0],zorder=1,lw=scale4*e11gXM))
ax.add_line(Line2D([firstCol,secondCol],[rows1[2],rows2[1]],color=heightColor[1],zorder=1,lw=scale4*e33gXM))
ax.add_line(Line2D([firstCol,secondCol],[rows1[5],rows2[2]],color=heightColor[2],zorder=1,lw=scale4*e13gYM))

ax.add_line(Line2D([secondCol,thirdCol-width/2.0],[rows2[0],rows3[0]+(height[2]+height[1])/2.0],color=heightColor[0],zorder=1,lw=max(height[0]*scale5,0.5)))
ax.add_line(Line2D([secondCol,thirdCol-width/2.0],[rows2[1],rows3[0]+(height[2]-height[0])/2.0],color=heightColor[1],zorder=1,lw=max(height[1]*scale5,0.5)))
ax.add_line(Line2D([secondCol,thirdCol-width/2.0],[rows2[2],rows3[0]-(height[1]+height[0])/2.0],color=heightColor[2],zorder=1,lw=max(height[2]*scale5,0.5)))
height = [0]*3
height[0]=v1111*e33gYM*scale3
height[1]=v1122*e11gYM*scale3
height[2]=2*v1212*e13gXM*scale3
heightColor = ['#965F56','#EB9486','#F2BAB2']
box(ax,r'flexo Ez',thirdCol,rows3[1],textShift3,width,height,heightColor)

ax.add_line(Line2D([firstCol,secondCol],[rows1[3],rows2[0]],color=heightColor[0],zorder=1,lw=scale4*e33gYM))
ax.add_line(Line2D([firstCol,secondCol],[rows1[1],rows2[1]],color=heightColor[1],zorder=1,lw=scale4*e11gYM))
ax.add_line(Line2D([firstCol,secondCol],[rows1[4],rows2[2]],color=heightColor[2],zorder=1,lw=scale4*e13gXM))

ax.add_line(Line2D([secondCol,thirdCol-width/2.0],[rows2[0],rows3[1]+(height[2]+height[1])/2.0],color=heightColor[0],zorder=1,lw=max(height[0]*scale5,0.5)))
ax.add_line(Line2D([secondCol,thirdCol-width/2.0],[rows2[1],rows3[1]+(height[2]-height[0])/2.0],color=heightColor[1],zorder=1,lw=max(height[1]*scale5,0.5)))
ax.add_line(Line2D([secondCol,thirdCol-width/2.0],[rows2[2],rows3[1]-(height[1]+height[0])/2.0],color=heightColor[2],zorder=1,lw=max(height[2]*scale5,0.5)))
ax.axis("equal")
ax.set_xlim(0,300)


plt.savefig(params['imgDir']+'/sup6.svg')

#%%

copyfile(params['imgDir']+'/sup6.svg',params['imgFile'])

fixSVGASCII(params['imgFile'])


with open(params['captionFile'],'w') as file:
    file.write(params['caption'])

















