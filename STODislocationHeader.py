# -*- coding: utf-8 -*-
"""
Created on Tue Apr  2 19:33:24 2019

@author: cxx
"""
import os
import sys
import platform
import h5py
import numpy as np

from math import sin,cos

import pandas as pd

from shutil import copyfile
from lxml import etree
    
from matplotlib.ticker import AutoMinorLocator


if platform.system() == 'Windows':
    newPath = 'D:\\SynologyDrive\\Code\\GitLab'
elif platform.system() == "Linux":
    newPath = '/gpfs/group/default/xuc116'
else:
    newPath = '/Users/xiaoxingcheng/SynologyDrive/Code/GitLab'

if newPath not in sys.path:
    print("Append new path",newPath)
    sys.path.append(newPath)
    
    
from muproData.research_header import *
from muproData.util import *
import muproData.readDat
# from muproData.inkscapeDrawing import InkscapeDrawing 

# import svgwrite
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from matplotlib.lines import Line2D
from matplotlib.patches import Rectangle
from matplotlib.patches import Circle
import matplotlib.lines as lines
import matplotlib.ticker as ticker
from matplotlib.text import Text

if platform.system() != 'Linux':
    import svgutils.compose as sc
    import svgutils.transform as st

pi=3.141592653589
from math import sqrt,cos,sin
from shutil import copy

#%%
def combineDictionary(dict1,dict2):
    out={}
    for key in dict1:
        if key in dict2:
            out[key] = dict2[key]
        else:
            out[key] = dict1[key]
    return out

def add_dislocation(fig,axis,x,y,color='white',angle=0):
    x1=x-0.05*cos(angle/180*pi)
    x2=x+0.05*cos(angle/180*pi)
    y1=y+0.05*sin(angle/180*pi)
    y2=y-0.05*sin(angle/180*pi)
    x3=x
    x4=x+0.08*sin(angle/180*pi)
    y3=y
    y4=y+0.08*cos(angle/180*pi)
    axis.add_line(lines.Line2D([x1,x2],[y1,y2],linewidth=3,transform=fig.transFigure,color=color))
    axis.add_line(lines.Line2D([x3,x4],[y3,y4],linewidth=3,transform=fig.transFigure,color=color))
    return axis

def add_horizontal_line(fig,axis,y,color='white'):
    axis.add_line(lines.Line2D([0,1],[y,y],linewidth=1,linestyle='--',transform=fig.transFigure,color=color))
    return axis
    
def heatPlot_noEdge(dataset,valRange=[],height=4,filename='',disx=0.5,disy=0.5,disColor='green',cmapGlob = 'viridis',linePos=0,disAngle=0,artists=[]):
    data = np.array(dataset)
    sizes = np.shape(data)     
    if valRange==[]:
        valMin=data.min()
        valMax=data.max()
    else:
        valMin=valRange[0]
        valMax=valRange[1]
        
    fig = plt.figure()
    fig.set_size_inches(height * sizes[0] / sizes[1], height, forward = False)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    
    ax.imshow(data.T,origin='lower',interpolation='bilinear', cmap=cmapGlob,vmin=valMin,vmax=valMax)
    if disAngle >= 0:
        ax=add_dislocation(fig,ax,disx,disy,color=disColor,angle=disAngle)
    for patch in artists:
        ax.add_artist(patch)
    if linePos != 0:
        ax = add_horizontal_line(fig,ax,linePos,color=disColor)
    if filename!='':
        plt.savefig(filename,dpi=72) 
    plt.show()
    plt.close()

def texEquation(equation,width=4,height=4,filename='',fs=15,posX=0.5,posY=1,va='top',ha='center',transparent=False):
        
    fig = plt.figure()
    fig.set_size_inches(width, height, forward = False)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    ax.text(posX,posY,equation,va=va,ha=ha,fontsize=fs)
    plt.gca().set_facecolor((1.0,0.47,0.42))
    if filename!='':
        plt.savefig(filename,dpi=72,transparent=transparent) 
    plt.show()
    plt.close()

def heatQuiverPlot_noEdge(dataset,datasetX,datasetY,valRange=[],height=4,scaleIn=10,filename='',disx=0.5,disy=0.5,disColor='green',cmapGlob = 'viridis',linePos=0.0,disAngle=0,artists = []):
    data = np.array(dataset)
    dataX = np.array(datasetX)
    dataY = np.array(datasetY)
    sizes = np.shape(data)     
    if valRange==[]:
        valMin=data.min()
        valMax=data.max()
    else:
        valMin=valRange[0]
        valMax=valRange[1]
        
    fig = plt.figure()
    fig.set_size_inches(height * sizes[0] / sizes[1], height, forward = False)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    
    ax.imshow(data.T,origin='lower',interpolation='bilinear', cmap=cmapGlob,vmin=valMin,vmax=valMax)
    ax.quiver(dataX.T,dataY.T,color='white',scale=scaleIn,width=0.005,headwidth=4,headlength=7,headaxislength=7,pivot='mid',minshaft=1.5,minlength=0.8)
    ax=add_dislocation(fig,ax,disx,disy,color=disColor,angle=disAngle)
    if isinstance(linePos,float) == True:
        if linePos != 0:
            ax = add_horizontal_line(fig,ax,linePos,color=disColor)
    else:
        ax.add_line(lines.Line2D([linePos[0],linePos[1]],[linePos[2],linePos[3]],linewidth=1,linestyle='--',transform=fig.transFigure,color=disColor))

    for patch in artists:
        ax.add_artist(patch)
    if filename!='':
        plt.savefig(filename,dpi=72) 
    plt.show()
    plt.close()
    
def plotColorBar(filename,label,valRange,height,width,pad_height,pad_width,fs,lformat='%+.1d',sciText='',cmapGlob = 'viridis'):
    fig,ax=plt.subplots()
#    fig.set_figheight(height)
#    fig.set_figwidth(width)
    fig.set_size_inches(width+pad_width, height+pad_height, forward = False)
    wid = width/(width+pad_width)
    hei = height/(height+pad_height)
    ax.set_position([0,0,wid,hei])
    norm=mpl.colors.Normalize(vmin=valRange[0],vmax=valRange[1])
    cb = mpl.colorbar.ColorbarBase(ax,cmap=cmapGlob,norm=norm,orientation='vertical')
    cb.set_label(label,size=fs)
    ax.tick_params(labelsize=fs)
    ax.yaxis.offsetText.set_fontsize(fs)
    ax.yaxis.set_major_formatter(mpl.ticker.FormatStrFormatter(lformat))
    if sciText!='':
        ax.text(0, 1.05, sciText, va='center',ha='left',size=fs,transform=ax.transAxes)

#    plt.tight_layout()
    plt.savefig(filename,dpi=300)

def scatterPlot(dataX,dataY,filename,label,color,marker,height,width,location,yrange=[-0.21,+0.21],fs=20,padding=[0.0,0.0,0.85,1.0],artists=[]):
    fig = plt.figure(figsize=(width,height))
    ax=fig.gca()
    ax.set_position(padding)
    for i  in range(len(label)):
        ax.plot(dataX[i],dataY[i],label=label[i],color=color[i],marker=marker[i])
        ax.yaxis.offsetText.set(fontsize=fs)
        ax.yaxis.tick_right()
        ax.set_ylim(yrange)
        ax.tick_params(labelsize=fs)
    plt.legend(loc=location,fancybox=True, framealpha=0.5,fontsize=fs)
#    plt.tight_layout()1
    for patch in artists:
        ax.add_artist(patch)
    plt.savefig(filename,dpi=300)
    plt.show()
    plt.close()
    
def getScaleBar(posX,posY,length,height,text,color = 'white',bg='None',lw = 2,fs = 20,padding = [2,2]):
    path_data = [
    (Path.MOVETO, (posX - length/2.0,posY + height)),
    (Path.LINETO, (posX - length/2.0,posY)),
    (Path.LINETO, (posX + length/2.0,posY)),
    (Path.LINETO, (posX + length/2.0,posY + height)),
    ]
    codes, verts = zip(*path_data)
    path = Path(verts, codes)
    patch = PathPatch(path, facecolor='None',edgecolor=color, alpha=1,lw=lw,zorder=98)
    patch2 = Text(posX, posY + height / 2.0,text,ha='center',zorder=99,color=color,fontsize=fs)
    outPatch = [patch,patch2]
    if bg != 'None':
        patch3 = Rectangle([posX - length/2.0 - padding[0],posY - height/2.0],length + padding[0]*2,height*2+padding[1]*2,fill=True,color=bg,zorder=97)
        outPatch.append(patch3)
    return outPatch
    