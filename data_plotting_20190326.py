# -*- coding: utf-8 -*-
"""
Created on Tue Mar 26 00:53:18 2019

@author: cxx
"""

import importlib
import sys
import os
import numpy as np
import numpy, scipy.optimize
import os
import matplotlib.pyplot as plt
sys.path.insert(0,'D:\SynologyDrive\Code\GitLab')
#sys.path.insert(0,'/Users/xiaoxingcheng/SynologyDrive/Code/GitLab')

import muproData.readDat
from muproData.util import *
#import muproData.svg_stack as ss
#import svgutils.transform as sg
import matplotlib as mpl
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import pandas as pd
import h5py
from muproData.research_header import *

os.chdir("D:/SynologyDrive/Research/STO-Dislocation-Charged")
#os.chdir("/Users/xiaoxingcheng/SynologyDrive/Research/STO-Dislocation-Flexo/paper")

#%%

def heatPlot(data,filename):
    plt.imshow(np.array(data).T,origin='lower')
    plt.colorbar()
    plt.savefig(filename)
    plt.close()

def processData():
    dataAllFile= h5py.File(params['processedDatDir'],'r')
    dataAll = dataAllFile[params['groupName']]
    batchFolders = list(dataAll.keys())
    
    for folder in batchFolders:
        var,index=parse_folder_name(folder)
        flexoCoefficient = var[params['batchKey'][0]]    
        noiseSeed = var[params['batchKey'][1]]
        data = dataAll[folder]
        imgDir = params['imgDir']+'/'+folder+'/'
        touch(imgDir)
        for dat in data.keys():
            heatPlot(data[dat],imgDir+dat+'.png')
    
    dataAllFile.close()
#%%
params={}

params['imgDir']='./img/burg100'
params['processedDatDir']='./20190507_STO_Dislocation_Charge.h5'
params['curDir']="D:/SynologyDrive/Research/STO-Dislocation-Charged"
params['start'] = 2000
params['polar'] = 'Polar.00002000.dat'
params['stress'] = 'Stress.00002001.dat'
params['flexo'] = 'Eflexo.00002001.dat'

params['nx']=512
params['ny']=1
params['nz']=512


params['batchKey']=['&MATERIAL.FLEXOCON','&PNOISEED','&QNOISEED']

#%%

params['imgDir']='./img/burg100'
params['groupName']='/burg100_no_charge/'
params['batchKey']=['&MATERIAL.FLEXOCON','&PNOISEED','&QNOISEED']
processData()

#%%
params['imgDir']='./img/burg110'
params['groupName']='/burg110_no_charge/'
params['batchKey']=['&MATERIAL.FLEXOCON','&PNOISEED','&QNOISEED']
processData()

#%%
params['imgDir']='./img/burg100_with_charge'
params['groupName']='/burg100_with_charge/'
params['batchKey']=['&DISLOCATION1.DEFECTCHARGE','&PNOISEED','QNOISEED']
processData()

#%%
params['imgDir']='./img/burg110_with_charge'
params['groupName']='/burg110_with_charge/'
params['batchKey']=['&DISLOCATION1.DEFECTCHARGE','&PNOISEED','QNOISEED']
processData()

#%%
params['imgDir']='./img/burg100_with_charge_no_flexo'
params['groupName']='/burg100_with_charge_no_flexo/'
params['batchKey']=['&DISLOCATION1.DEFECTCHARGE','&PNOISEED','QNOISEED']
processData()

#%%
params['imgDir']='./img/burg110_with_charge_no_flexo'
params['groupName']='/burg110_with_charge_no_flexo/'
params['batchKey']=['&DISLOCATION1.DEFECTCHARGE','&PNOISEED','QNOISEED']
processData()

#%%
params['imgDir']='./img/screw110_with_charge_no_flexo'
params['groupName']='/burg110_with_charge_no_flexo/'
params['batchKey']=['&DISLOCATION1.DEFECTCHARGE','&PNOISEED','QNOISEED']
processData()
