# STO-Dislocation-Flexo

Data processing and plotting code for the manuscript about flexoelectric effect in STO  with dislocation

# List of figures
1. Stress distribution, stress gradient distribution
2. polarization with and without flexo, flexo field distribution, polarization line profile, experimental figure
3. Varying flexoelectric coefficient
4. 110 dislocation and with/without charge, statistics of polarization in different cases.
## suplimentary
1. Simulation setup
2. analyical and simulation comparison
3. 