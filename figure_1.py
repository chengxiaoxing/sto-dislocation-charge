# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 14:17:47 2019

@author: cxx
"""
import importlib
import sys
import os
import platform

if platform.system() == 'Windows':
    os.chdir("D:/SynologyDrive/Research/STO-Dislocation-Charged")
    newPath = os.getcwd()+'\\code'
elif platform.system() == 'Linux':
    os.chdir('/gpfs/scratch/xuc116/charge_flexo_sto_dislocation')
    newPath = os.getcwd()+'/code'
else:
    os.chdir('/Users/xiaoxingcheng/SynologyDrive/Research/STO-Dislocation-Charged')    
    newPath = os.getcwd()+'/code'

if newPath not in sys.path:
    print("Append new path")
    sys.path.append(newPath)
    
from STODislocationHeader import *

#%%

params={}

params['img'] = 'figure1'

params['datDir']='/burg_100/37+&MATERIAL.FLEXOCON_0.08_2.6_2.2+&DISLOCATION1.DEFECTCHARGE_0.6+pqrseed_1/'
params['initDatDir']='/burg_100_initial/'
params['file'] = '20190507_STO_Dislocation_Charge.h5'
params['fileInitial'] = '20190528_STO_Dislocation_Charge_Initial.h5'
params['nx']=512
params['ny']=1
params['nz']=512

params['xrange']=[244,268]
params['yrange']=[114,138]

params['disLocX']=0.5
params['disLocY']=0.5

#params['initDatDir']='/burg_110_initial/'
#
#params['disLocX']=0.49
#params['disLocY']=0.49
#
#params['xrange']=[115,139]
#params['yrange']=[115,139]

params['batchKey']=['&MATERIAL.FLEXOCON','&DISLOCATION1.DEFECTCHARGE','pqrseed']

params['caption'] = """

## Figure 1
![Figure 1](figure1.svg)

Stress and strain gradient distribution around dislocation core. 
(a) stress11, (b) stress33, (c) stress13, 
(d) strain 11 gradient, (e) strain 33 gradient, (f) strain 13 gradient.

"""

    
#%%
def makePlot(params):
#%%
    params['imgDir']='./img/'+params['img']
    params['imgFile'] = './img/'+params['img']+'.svg'
    params['ImgDir']='\\img\\'+params['img']+'\\'
    params['captionFile'] = './img/'+params['img']+'.md'
    
    touch(params['imgDir'])
    x1=params['xrange'][0]
    x2=params['xrange'][1]
    y1=params['yrange'][0]
    y2=params['yrange'][1]
    
    #%% open the file, important always remember to close it
    #dataAll= h5py.File('20190325_STO_dislocation_flexo.h5','r')
    # be careful with the gradient, since we store the data in x,y,z order, but when plotting, the 2D array is mapped directly to the image, 
    # the x and y axis needs to be reversed, so the gradient we calculated using the original data needs to be switched when used.
    dataAll= h5py.File(params['fileInitial'],'r')
    stress11 = np.array(dataAll[params['initDatDir']+'stress11'][x1:x2,y1:y2])

    density = 1
    # electric field
    elec1 = np.array(dataAll[params['initDatDir']+'electricField1'][x1:x2,y1:y2])
    elec3 = np.array(dataAll[params['initDatDir']+'electricField3'][x1:x2,y1:y2])
    phi = np.array(dataAll[params['initDatDir']+'electricPotential'][x1:x2,y1:y2])
    elecLength = np.sqrt(np.square(np.array(elec1))+np.square(np.array(elec3)))
    
    dataAll.close()
    
    dataAll= h5py.File(params['file'],'r')
    
    density = 1
    # electric field
    px = np.array(dataAll[params['datDir']+'px'][x1:x2,y1:y2])
    pz = np.array(dataAll[params['datDir']+'pz'][x1:x2,y1:y2])
    pLength = np.sqrt(np.square(np.array(px))+np.square(np.array(pz)))
    
    dataAll.close()
    
    
    exp_pxA = pd.read_excel('./exp_data.xlsx','Ps of A-site')
    exp_pxB = pd.read_excel('./exp_data.xlsx','Ps of B-site')
    haadf = Image.open('./exp_gb.png').convert("RGBA")
    draw = ImageDraw.Draw(haadf)
    expx0=2200
    expx1=4600
    expy0=100
    expy1=1500

    phiMax = abs(phi).max()
    phiRange = (-phiMax,phiMax)
    
    elecMax = abs(elecLength).max()
    elecRange = (-elecMax,elecMax)
    
    pMax = abs(pLength).max()
    pRange = (0,pMax)
    
    stressMax = abs(stress11).max()
    stressRange = (-stressMax,stressMax)
    #%%
    
    patch = getScaleBar(4,1,5,1.5,'2nm',color='white',lw=5,fs=30,padding=[1,1],bg = 'None')
    heatPlot_noEdge(stress11,stressRange,5,params['imgDir']+'/figure_1_stress.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,artists=patch)
    plotColorBar(params['imgDir']+'/figure_1_stress_colorBar.svg','Stress (Pa) ',[a/1e10 for a in stressRange],4.6,0.7,0.4,1.3,20,'%+0.2f','1e10',cmapGlob=cmapGlob)

    #%% sub figure a,b,c the stress 11, 33, 13, range from [-5e10,5e10]
    cmapGlob = 'inferno'
    
    
    patch = getScaleBar(4,1,5,1.5,'2nm',color='white',lw=5,fs=30,padding=[1,1],bg = 'None')
    heatPlot_noEdge(phi,phiRange,5,params['imgDir']+'/figure_1a_phi.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,artists=patch)
    plotColorBar(params['imgDir']+'/figure_1_phi_colorBar.svg','Phi (V)',[a/1 for a in phiRange],5.0,0.7,0.0,1.3,20,'%+0.1f','',cmapGlob=cmapGlob)
   
    #%% sub figure d,e,f the stress gradient, range from []
    cmapGlob = 'inferno'
    
    
    scale = elecLength.max()*15
    
    #scale =1e11
    times=1
    patch = [ Text(23, 23,'x'+str(times),va='top',ha='right',zorder=99,color='white',fontsize=30)]
    heatQuiverPlot_noEdge(elecLength,elec1,elec3,elecRange,5,scale,params['imgDir']+'/figure_1b_elecField.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,artists=patch)
    plotColorBar(params['imgDir']+'/figure_1_elecField_colorBar.svg','E (V/m)',[a/1.0e8 for a in elecRange],4.6,0.7,0.4,1.3,20,'%+.1f','1e8',cmapGlob)
    
    
    #%%
    
    cmapGlob = 'inferno'
    
    scale =5
    times = 2
    patch = [ Text(23, 23,'x'+str(times),va='top',ha='right',zorder=99,color='white',fontsize=30)]
    heatQuiverPlot_noEdge(pLength,px,pz,pRange,5,scale/times,params['imgDir']+'/figure_1c_p.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,artists=patch)
    plotColorBar(params['imgDir']+'/figure_1_p_colorBar.svg','P (C/m^2)',[a/1 for a in pRange],5.0,0.7,0.0,1.3,20,'%+.2f','',cmapGlob)
    
    
    #%%
#    texEquation(r'$\sigma_{11}$',0.8,0.4,params['imgDir']+'/sigma11.svg',20)
#    texEquation(r'$\sigma_{33}$',0.8,0.4,params['imgDir']+'/sigma33.svg',20)
#    texEquation(r'$\sigma_{13}$',0.8,0.4,params['imgDir']+'/sigma13.svg',20)
#    texEquation(r'$\nabla\epsilon_{11}$',0.8,0.4,params['imgDir']+'/nabla_sigma11.svg',20)
#    texEquation(r'$\nabla\epsilon_{33}$',0.8,0.4,params['imgDir']+'/nabla_sigma33.svg',20)
#    texEquation(r'$\nabla\epsilon_{13}$',0.8,0.4,params['imgDir']+'/nabla_sigma13.svg',20)
    #%%
    
    texEquation(r'a',0.4,0.4,'./img/a.svg',20,va='center',ha='center',posX = 0.5,posY=0.5)
    texEquation(r'b',0.4,0.4,'./img/b.svg',20,va='center',ha='center',posX = 0.5,posY=0.5)
    texEquation(r'c',0.4,0.4,'./img/c.svg',20,va='center',ha='center',posX = 0.5,posY=0.5)
    texEquation(r'd',0.4,0.4,'./img/d.svg',20,va='center',ha='center',posX = 0.5,posY=0.5)
    
    trans = True
    texEquation(r'a',0.4,0.4,'./img/aa.svg',20,va='center',ha='center',posX = 0.5,posY=0.5,transparent=trans)
    texEquation(r'b',0.4,0.4,'./img/bb.svg',20,va='center',ha='center',posX = 0.5,posY=0.5,transparent=trans)
    texEquation(r'c',0.4,0.4,'./img/cc.svg',20,va='center',ha='center',posX = 0.5,posY=0.5,transparent=trans)
    texEquation(r'd',0.4,0.4,'./img/dd.svg',20,va='center',ha='center',posX = 0.5,posY=0.5,transparent=trans)
    
    cropped_haadf = haadf.crop((expx0,expy0,expx1,expy1))
    cropped_haadf.save(params['imgDir']+'/cropped_haadf.png','PNG')
    draw.rectangle((expx0,expy0,expx1,expy1),outline="red",fill='white')
    haadf.save('./labeld.png',"PNG")
    
    image=plt.imread('./img/figure1/figure_1d.png')
    plt.axis('off')
    plt.imsave('./img/figure1/figure_1d_exp.svg',image)
    plt.close()

    #%% Combine the individual figure files
    #figures=[[params['ImgDir']+'figure_1a_stress11.svg',params['ImgDir']+'figure_1a_stress33.svg',params['ImgDir']+'figure_1a_stress13.svg',params['ImgDir']+'figure_1-stressColorBar.svg'],
    #         [params['ImgDir']+'figure_1d_sGrad11.svg',params['ImgDir']+'figure_1d_sGrad33.svg',params['ImgDir']+'figure_1d_sGrad33.svg',params['ImgDir']+'figure_1-stressGradientColorBar.svg']]
    #figureName=[['figure_1a_stress11.svg','figure_1b_stress33.svg','figure_1c_stress13.svg','figure_1-stressColorBar.svg'],
    #            ['sigma11.svg','sigma33.svg','sigma13.svg',''],
    #            ['figure_1d_sGrad11.svg','figure_1e_sGrad33.svg','figure_1f_sGrad13.svg','figure_1-stressGradientColorBar.svg'],
    #            ['nabla_sigma11.svg','nabla_sigma33.svg','nabla_sigma13.svg','']]
    
    figureName=[['figure_1a_phi.svg','figure_1_phi_ColorBar.svg','figure_1b_elecField.svg','figure_1_elecField_ColorBar.svg'],
                ['','','',''],
                ['figure_1c_p.svg','figure_1_p_ColorBar.svg','figure_1d_exp.svg',''],
                ['','','','']]
    figureWidth = 190
    labelWidth = 80
    figureHeight = 205
    textSize = "20"
    textBase = 18
    textLeft = 5
    boxSize="25"
    offsetY = 150
    figures = [[os.getcwd()+params['ImgDir']+figureName[i][j] for j in range(0,4)] for i in range(0,4)]
    label = [os.getcwd()+'\\img\\'+ name+'.svg' for name in ['a','b','c','d']]
    labelFont = 'DejaVu Sans'
    labelWeight = 'normal'
    sc.Figure('17cm','11cm',
              sc.Panel(
                      sc.SVG(figures[0][0]).scale(0.5),
                      sc.SVG(label[0]).move(-1,-1)
                      ),
              sc.Panel(
                      sc.SVG(figures[0][1]).scale(0.5).move(figureWidth,0),
#                      sc.SVG(label[1]).move(figureWidth-1,0-1)
                      ),
              sc.Panel(
                      sc.SVG(figures[0][2]).scale(0.5).move(figureWidth+labelWidth,0),
                      sc.SVG(label[1]).move(figureWidth+labelWidth-1,0-1)
                      ),
              sc.Panel(
                      sc.SVG(figures[0][3]).scale(0.5).move(figureWidth*2+labelWidth,0)
                      ),
#              sc.Panel(
#                      sc.SVG(figures[1][0]).move(72+figureWidth*0,offsetY)
#                      ),          
#              sc.Panel(
#                      sc.SVG(figures[1][1]).move(72+figureWidth*1,offsetY)
#                      ),
#              sc.Panel(
#                      sc.SVG(figures[1][2]).move(72+figureWidth*2,offsetY)
#                      ),
              sc.Panel(
                      sc.SVG(figures[2][0]).scale(0.5).move(0,figureHeight),
                      sc.SVG(label[2]).move(0-1,figureHeight-1)
                      ),
              sc.Panel(
                      sc.SVG(figures[2][1]).scale(0.5).move(figureWidth,figureHeight),
#                      sc.SVG(label[4]).move(figureWidth-1,figureHeight-1)
                      ),
              sc.Panel(
                      sc.SVG(figures[2][2]).scale(0.5).move(figureWidth+labelWidth,figureHeight),
                      sc.SVG(label[3]).move(figureWidth+labelWidth-1,figureHeight-1)
                      )
#              sc.Panel(
#                      sc.SVG(figures[2][3]).scale(0.5).move(figureWidth*3,figureHeight)
#                      ),
#              sc.Panel(
#                      sc.SVG(figures[3][0]).move(72+figureWidth*0,offsetY+figureHeight)
#                      ),          
#              sc.Panel(
#                      sc.SVG(figures[3][1]).move(72+figureWidth*1,offsetY+figureHeight)
#                      ),
#              sc.Panel(
#                      sc.SVG(figures[3][2]).move(72+figureWidth*2,offsetY+figureHeight)
#                      )
              ).save(params['imgFile'])
    #          sc.Grid(20,20)
              
    #%%
    fixSVGASCII(params['imgFile'])
    with open(params['captionFile'],'w') as file:
        file.write(params['caption'])
        
        
#%%
if __name__ == "__main__":
    print("You are running the script on its own.")
    makePlot(params)
else:
    print("You are importing the script from another file, call combineVariable(sourceParams,replaceParams), then call makePlot(params)")
#    params=combineVariable(params,globalParams)